package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	game "slotGame/tooRich/game"
	"strconv"
	"time"
	tool "tools"

	"github.com/Luxurioust/excelize"
)

// UpperLimitRTP 上限
var UpperLimitRTP float64 = 0.969

// LowerLimitRTP 下限
var LowerLimitRTP float64 = 0.961

// HallUpperLimitRTP 水池上限
var HallUpperLimitRTP float64 = 0.969

// HallLowerLimitRTP 水池下限
var HallLowerLimitRTP float64 = 0.961

// // InitialWaterLoopBet 重置押注
// var InitialWaterLoopBet float64 = 2500000

// // InitialWaterLoopPay 重置派彩
// var InitialWaterLoopPay float64 = 2412500

// // LimitPayOff 最大派彩
// var LimitPayOff float64 = 100000

// // DayGroup 最大派彩條件幾天一組
// var DayGroup int = 14

func main() {
	// try()
	// GetData()
	// HallRiskControl()

	for ii := 0; ii < 1; ii++ {
		// RiskControlSingleBet(100000000, 14, 100000)
		var betSlice []int = generateBetSlice(100000000)
		fmt.Println("-------betSlice_DONE-------")

		// var tmp1 []int
		// var tmp2 []int
		// var tmp3 []int
		// for jj := 0; jj < 50; jj++ {
		// 	tmp1 = append(tmp1, RiskControlRandomBet(betSlice, 7, 100000, 250000, 241250))
		// 	tmp2 = append(tmp2, RiskControlRandomBet(betSlice, 7, 100000, 500000, 482500))
		// 	tmp3 = append(tmp3, RiskControlRandomBet(betSlice, 7, 100000, 2500000, 2412500))
		// }
		// sort.Ints(tmp1)
		// sort.Ints(tmp2)
		// sort.Ints(tmp3)
		// fmt.Println(tmp1)
		// fmt.Println(tmp2)
		// fmt.Println(tmp3)
		// fmt.Println("-------RiskControlRandomBet_7天_DONE-------")

		// for jj := 0; jj < 50; jj++ {
		// 	tmp1 = append(tmp1, RiskControlRandomBet(betSlice, 14, 100000, 250000, 241250))
		// 	tmp2 = append(tmp2, RiskControlRandomBet(betSlice, 14, 100000, 500000, 482500))
		// 	tmp3 = append(tmp3, RiskControlRandomBet(betSlice, 14, 100000, 2500000, 2412500))
		// }
		// sort.Ints(tmp1)
		// sort.Ints(tmp2)
		// sort.Ints(tmp3)
		// fmt.Println(tmp1)
		// fmt.Println(tmp2)
		// fmt.Println(tmp3)
		// fmt.Println("-------RiskControlRandomBet_14天_DONE-------")

		// for jj := 0; jj < 50; jj++ {
		// 	tmp1 = append(tmp1, RiskControlRandomBet(betSlice, 21, 100000, 250000, 241250))
		// 	tmp2 = append(tmp2, RiskControlRandomBet(betSlice, 21, 100000, 500000, 482500))
		// 	tmp3 = append(tmp3, RiskControlRandomBet(betSlice, 21, 100000, 2500000, 2412500))
		// }
		// sort.Ints(tmp1)
		// sort.Ints(tmp2)
		// sort.Ints(tmp3)
		// fmt.Println(tmp1)
		// fmt.Println(tmp2)
		// fmt.Println(tmp3)
		// fmt.Println("-------RiskControlRandomBet_21天_DONE-------")

		RiskControlRandomBet(betSlice, 7, 100000, 250000, 241250)
		RiskControlRandomBet(betSlice, 14, 100000, 250000, 241250)
		RiskControlRandomBet(betSlice, 21, 100000, 250000, 241250)
		// fmt.Println("-------RiskControlRandomBet_1_DONE-------")
		// RiskControlRandomBet(betSlice, 7, 100000, 500000, 482500)
		// RiskControlRandomBet(betSlice, 14, 100000, 500000, 482500)
		// RiskControlRandomBet(betSlice, 21, 100000, 500000, 482500)
		// fmt.Println("-------RiskControlRandomBet_2_DONE-------")
		// RiskControlRandomBet(betSlice, 7, 100000, 2500000, 2412500)
		// RiskControlRandomBet(betSlice, 14, 100000, 2500000, 2412500)
		// RiskControlRandomBet(betSlice, 21, 100000, 2500000, 2412500)
		// fmt.Println("-------RiskControlRandomBet_3_DONE-------")
	}
}

// HallRiskControl 依照廳別處理風控
func HallRiskControl(limitPayOff float64, _initialWaterLoopBet float64, _initialWaterLoopPay float64) {
	var result7 []HallResult
	var result14 []HallResult
	var dayGroup7 int = 7
	var dayGroup14 int = 14

	// 計算總rtp_不分廳
	var allSpins7 int = 0
	var allBet7 float64 = 0
	var allPay7 float64 = 0
	var allSpins14 int = 0
	var allBet14 float64 = 0
	var allPay14 float64 = 0
	var TotalCount int = 0
	var count int = 0

	// 寫進excel
	f := excelize.NewFile()
	// 創建一個工作表
	index := f.NewSheet("Sheet2")

	for hallIndex := range hallGroup {
		count++
		fmt.Println("廳計數: ", count)
		fmt.Println(len(hallGroup[hallIndex]))
		TotalCount += len(hallGroup[hallIndex])
		var tmp7 = RiskControl(len(hallGroup[hallIndex]), hallGroup[hallIndex], hallIndex, dayGroup7, limitPayOff, _initialWaterLoopBet, _initialWaterLoopPay)
		result7 = append(result7, tmp7)
		allSpins7 += tmp7.Spins
		allBet7 += tmp7.TotalBet
		allPay7 += tmp7.TotalPay

		var tmp14 = RiskControl(len(hallGroup[hallIndex]), hallGroup[hallIndex], hallIndex, dayGroup14, limitPayOff, _initialWaterLoopBet, _initialWaterLoopPay)
		result14 = append(result14, tmp14)
		allSpins14 += tmp14.Spins
		allBet14 += tmp14.TotalBet
		allPay14 += tmp14.TotalPay
		fmt.Println("============================================================================")
	}

	fmt.Println("@基本數值")
	fmt.Println("最大派彩限制:        ", limitPayOff)
	fmt.Println("最大派彩限制天數循環: ", dayGroup7, dayGroup14)
	fmt.Println("RTP上限:            ", UpperLimitRTP)
	fmt.Println("RTP下限:            ", LowerLimitRTP)
	fmt.Println("allSpins7/14:      ", allSpins7, allSpins14, "====", TotalCount)
	fmt.Println("總RTP7/14:          ", allPay7/allBet7, allPay14/allBet14)
	fmt.Println("allBet7/14:        ", allBet7, allBet14)
	fmt.Println("allPay7/14:         ", allPay7, allPay14)
	fmt.Println("==================")

	for h := 0; h < len(result7); h++ {
		fmt.Println("Count", h+1)
		fmt.Println("hallID:        ", result7[h].HallID, result14[h].HallID)
		fmt.Println("spins:         ", result7[h].Spins, result14[h].Spins)
		fmt.Println("totalBet:      ", result7[h].TotalBet, result14[h].TotalBet)
		fmt.Println("totalPay:      ", result7[h].TotalPay, result14[h].TotalPay)
		fmt.Println("AverangeRTP:   ", result7[h].AverangeRTP, result14[h].AverangeRTP)
		fmt.Println("RTP95:         ", result7[h].RTP95, result14[h].RTP95)
		fmt.Println("RTP965:        ", result7[h].RTP965, result14[h].RTP965)
		fmt.Println("RTP99:         ", result7[h].RTP99, result14[h].RTP99)
		fmt.Println("LimitPayCount: ", result7[h].LimitPayCount, result14[h].LimitPayCount)
		fmt.Println("-------------------------------------------------")
		fmt.Println("-------------------------------------------------")

		// 設置存儲格的值
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+1), "HallID")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+2), "局數")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+3), "總下注")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+4), "總得分")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+5), "平均 RTP")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+6), "RTP95佔比")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+7), "RTP965佔比")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+8), "RTP99佔比")
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(h*10+9), "被黨最大派彩次數")

		// 設置存儲格的值_7天
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+1), result7[h].HallID)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+2), result7[h].Spins)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+3), result7[h].TotalBet)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+4), result7[h].TotalPay)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+5), result7[h].AverangeRTP)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+6), result7[h].RTP95)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+7), result7[h].RTP965)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+8), result7[h].RTP99)
		f.SetCellValue("Sheet2", "B"+strconv.Itoa(h*10+9), result7[h].LimitPayCount)

		// 設置存儲格的值_14天
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+1), result14[h].HallID)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+2), result14[h].Spins)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+3), result14[h].TotalBet)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+4), result14[h].TotalPay)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+5), result14[h].AverangeRTP)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+6), result14[h].RTP95)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+7), result14[h].RTP965)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+8), result14[h].RTP99)
		f.SetCellValue("Sheet2", "C"+strconv.Itoa(h*10+9), result14[h].LimitPayCount)
	}
	// 設置活頁簿的默認工作表
	f.SetActiveSheet(index)
	// 根據指定路徑保存活頁簿
	if err := f.SaveAs("Book1.xlsx"); err != nil {
		fmt.Println(err)
	}
}

// RiskControl 風控
func RiskControl(playRound int, hallData [][]string, hallID string, dayGroup int, limitPayOff float64, _initialWaterLoopBet float64, _initialWaterLoopPay float64) HallResult {
	spendTime := time.Now()

	var hallResult HallResult
	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = playRound
	var unitBet float64 = 10
	var totalBet float64 = 0
	var bet float64 = 0
	var gold float64 = 0
	var exchangeRate float64 = 0
	var NGScore float64 = 0
	var FGScore float64 = 0
	var result = game.GameResult{}
	var waterLoopBet = _initialWaterLoopBet
	var waterLoopPay = _initialWaterLoopPay
	var accumulationBet float64 = 0
	rtpmodule := 965
	var hallWaterLoopPay = []float64{}
	var hallWaterLoopBet = []float64{}

	var hallWaterLoopPayOneDay float64
	var hallWaterLoopBetOneDay float64
	var intoFG int = 0

	var moduleCount = []int{0, 0, 0}
	// 被擋做大派彩次數
	var limitPayCount int = 0

	// var bar string = ""

	for time := 0; time < round; {
		// 顯示進度
		// if time%(round/20) == 0 && time != 0 {
		// 	// print("=")
		// 	bar += "█"
		// 	fmt.Printf("\r[%-20s]", bar)
		// }

		rtpmodule = GetRtpmodule(waterLoopPay, waterLoopBet)
		//rtpmodule = 2
		if rtpmodule == 95 {
			moduleCount[0]++
		} else if rtpmodule == 965 {
			moduleCount[1]++
		} else if rtpmodule == 99 {
			moduleCount[2]++
		}
		// moduleCount[rtpmodule]++

		if result.Status == "" || result.Status == "NORMAL" {
			// 決定 Bet、Gold、ExchangeRate
			tmpBet, err1 := strconv.ParseFloat(hallData[time][3], 64)
			if err1 != nil {
				fmt.Println("WrongBet: ", tmpBet)
			}
			tmpExchangeRate, err2 := strconv.ParseFloat(hallData[time][6], 64)
			if err2 != nil {
				fmt.Println("WrongBet: ", tmpExchangeRate)
			}
			bet = tmpBet
			gold = (bet / unitBet)
			exchangeRate = tmpExchangeRate

			for {
				result = game.GetNormalGameInfo(rtpmodule)
				// 需要乘上匯率
				if float64(result.TotalWin)*gold*exchangeRate > limitPayOff {
					if GetlimitPayout(hallWaterLoopPayOneDay+float64(result.TotalWin)*gold*exchangeRate, hallWaterLoopBetOneDay, hallWaterLoopPay, hallWaterLoopBet, dayGroup) {
						break
					}
				} else {
					break
				}
				fmt.Println("MainGame_派彩超過", limitPayOff, ": ", float64(result.TotalWin)*gold*exchangeRate)
				limitPayCount++
			}
			NGScore += (float64(result.TotalWin) * gold * exchangeRate)
			totalBet += (bet * exchangeRate)
			hallWaterLoopBetOneDay += (bet * exchangeRate)
			waterLoopBet += (bet * exchangeRate)
			accumulationBet += (bet * exchangeRate)
			//moduleCount[rtpmodule]++
			if result.Status == "FREE" {
				intoFG++
			}
			time++
		} else {
			for {
				result = game.GetFreeGameInfo()
				if (float64(result.TotalWin) * gold * exchangeRate) > limitPayOff {
					if GetlimitPayout(hallWaterLoopPayOneDay+float64(result.TotalWin)*gold*exchangeRate, hallWaterLoopBetOneDay, hallWaterLoopPay, hallWaterLoopBet, dayGroup) {
						break
					}
				} else {
					break
				}
				fmt.Println("FreeGame_派彩超過", limitPayOff, ": ", float64(result.TotalWin)*gold*exchangeRate)
				limitPayCount++
			}
			FGScore += (float64(result.TotalWin) * gold * exchangeRate)
		}

		waterLoopPay += (float64(result.TotalWin) * gold * exchangeRate)
		hallWaterLoopPayOneDay += (float64(result.TotalWin) * gold * exchangeRate)

		if accumulationBet > 1200000 {
			waterLoopBet = _initialWaterLoopBet
			waterLoopPay = _initialWaterLoopPay
			accumulationBet = 0
		} else if accumulationBet > 800000 {
			if waterLoopPay/waterLoopBet >= LowerLimitRTP &&
				waterLoopPay/waterLoopBet <= UpperLimitRTP {
				waterLoopBet = _initialWaterLoopBet
				waterLoopPay = _initialWaterLoopPay
			}
		}
		if hallWaterLoopBetOneDay > 500000 {
			hallWaterLoopPay = append(hallWaterLoopPay, hallWaterLoopPayOneDay)
			hallWaterLoopBet = append(hallWaterLoopBet, hallWaterLoopBetOneDay)
			hallWaterLoopPayOneDay = 0
			hallWaterLoopBetOneDay = 0
		}

		//fmt.Println("")
	}

	totalRTP := float64(NGScore+FGScore) / float64(totalBet)
	fmt.Println(totalRTP)

	hallResult.HallID = hallID
	hallResult.Spins = round
	hallResult.TotalBet = totalBet
	hallResult.TotalPay = NGScore + FGScore
	hallResult.AverangeRTP = totalRTP
	hallResult.RTP95 = float64(moduleCount[0]) / float64(round)
	hallResult.RTP965 = float64(moduleCount[1]) / float64(round)
	hallResult.RTP99 = float64(moduleCount[2]) / float64(round)
	hallResult.LimitPayCount = limitPayCount

	fmt.Println("@基本數值")
	fmt.Println("最大派彩限制:        ", limitPayOff)
	// fmt.Println("最大派彩限制天數循環: ", DayGroup)
	fmt.Println("RTP上限:           ", UpperLimitRTP)
	fmt.Println("RTP下限:           ", LowerLimitRTP)
	fmt.Println("重置押注:           ", _initialWaterLoopBet)
	fmt.Println("重置派彩:           ", _initialWaterLoopPay)
	fmt.Println("總RTP:             ", totalRTP)
	fmt.Println("NG_RTP:            ", (NGScore)/(totalBet))
	fmt.Println("FG_RTP:            ", (FGScore)/(totalBet))
	fmt.Println("round:             ", round)
	fmt.Println("TotalBet:          ", totalBet)
	fmt.Println("NGScore:           ", NGScore)
	fmt.Println("FGScore:           ", FGScore)
	fmt.Println("intoFG:            ", intoFG)
	fmt.Println("moduleCount:       ", moduleCount)
	// fmt.Println("hallwaterlooppay:  ", hallWaterLoopPay)
	// fmt.Println("hallwaterloopbet:  ", hallWaterLoopBet)

	elapsed := time.Since(spendTime)
	fmt.Println("＠花費時間: ", elapsed)
	fmt.Println("------------------------")
	fmt.Println("------------------------")

	return hallResult
}

// RiskControlSingleBet 風控單一下注
func RiskControlSingleBet(playRound int, dayGroup int, limitPayOff float64, _initialWaterLoopBet float64, _initialWaterLoopPay float64) {
	spendTime := time.Now()

	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = playRound
	var unitBet float64 = 10
	var totalBet float64 = 0
	var bet float64 = 0
	var gold float64 = 0
	var exchangeRate float64 = 0
	var NGScore float64 = 0
	var FGScore float64 = 0
	var result = game.GameResult{}
	var waterLoopBet = _initialWaterLoopBet
	var waterLoopPay = _initialWaterLoopPay
	var accumulationBet float64 = 0
	rtpmodule := 965
	var hallWaterLoopPay = []float64{}
	var hallWaterLoopBet = []float64{}

	var hallWaterLoopPayOneDay float64
	var hallWaterLoopBetOneDay float64
	var intoFG int = 0

	var moduleCount = []int{0, 0, 0}
	// 被擋做大派彩次數
	var limitPayCount int = 0

	var bar string = ""

	for time := 0; time < round; {
		// 顯示進度
		if time%(round/20) == 0 && time != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-20s]", bar)
		}

		rtpmodule = GetRtpmodule(waterLoopPay, waterLoopBet)

		if result.Status == "" || result.Status == "NORMAL" {
			bet = 100
			gold = (bet / unitBet)
			exchangeRate = 1
			if rtpmodule == 95 {
				moduleCount[0]++
			} else if rtpmodule == 965 {
				moduleCount[1]++
			} else if rtpmodule == 99 {
				moduleCount[2]++
			}

			for {
				result = game.GetNormalGameInfo(rtpmodule)
				// 需要乘上匯率
				if float64(result.TotalWin)*gold*exchangeRate > limitPayOff {
					if GetlimitPayout(hallWaterLoopPayOneDay+float64(result.TotalWin)*gold*exchangeRate, hallWaterLoopBetOneDay, hallWaterLoopPay, hallWaterLoopBet, dayGroup) {
						break
					}
				} else {
					break
				}
				// fmt.Println("MainGame_派彩超過", LimitPayOff, ": ", float64(result.TotalWin)*gold*exchangeRate)
				limitPayCount++
			}
			NGScore += (float64(result.TotalWin) * gold * exchangeRate)
			totalBet += (bet * exchangeRate)
			hallWaterLoopBetOneDay += (bet * exchangeRate)
			waterLoopBet += (bet * exchangeRate)
			accumulationBet += (bet * exchangeRate)
			//moduleCount[rtpmodule]++
			if result.Status == "FREE" {
				intoFG++
			}
			time++
		} else {
			for {
				result = game.GetFreeGameInfo()
				if (float64(result.TotalWin) * gold * exchangeRate) > limitPayOff {
					if GetlimitPayout(hallWaterLoopPayOneDay+float64(result.TotalWin)*gold*exchangeRate, hallWaterLoopBetOneDay, hallWaterLoopPay, hallWaterLoopBet, dayGroup) {
						break
					}
				} else {
					break
				}
				// fmt.Println("FreeGame_派彩超過", limitPayOff, ": ", float64(result.TotalWin)*gold*exchangeRate)
				limitPayCount++
			}
			FGScore += (float64(result.TotalWin) * gold * exchangeRate)
		}

		waterLoopPay += (float64(result.TotalWin) * gold * exchangeRate)
		hallWaterLoopPayOneDay += (float64(result.TotalWin) * gold * exchangeRate)

		if accumulationBet > 1200000 {
			waterLoopBet = _initialWaterLoopBet
			waterLoopPay = _initialWaterLoopPay
			accumulationBet = 0
		} else if accumulationBet > 800000 {
			if waterLoopPay/waterLoopBet >= LowerLimitRTP &&
				waterLoopPay/waterLoopBet <= UpperLimitRTP {
				waterLoopBet = _initialWaterLoopBet
				waterLoopPay = _initialWaterLoopPay
			}
		}
		if hallWaterLoopBetOneDay > 500000 {
			hallWaterLoopPay = append(hallWaterLoopPay, hallWaterLoopPayOneDay)
			hallWaterLoopBet = append(hallWaterLoopBet, hallWaterLoopBetOneDay)
			hallWaterLoopPayOneDay = 0
			hallWaterLoopBetOneDay = 0
		}

		//fmt.Println("")
	}

	totalRTP := float64(NGScore+FGScore) / float64(totalBet)
	fmt.Println(totalRTP)

	fmt.Println("@基本數值")
	fmt.Println("最大派彩限制:        ", limitPayOff)
	fmt.Println("最大派彩限制天數循環: ", dayGroup)
	fmt.Println("重置押注:           ", _initialWaterLoopBet)
	fmt.Println("重置派彩:           ", _initialWaterLoopPay)
	fmt.Println("Bet:              ", bet)
	fmt.Println("RTP上限:           ", UpperLimitRTP)
	fmt.Println("RTP下限:           ", LowerLimitRTP)
	fmt.Println("總RTP:             ", totalRTP)
	fmt.Println("NG_RTP:            ", (NGScore)/(totalBet))
	fmt.Println("FG_RTP:            ", (FGScore)/(totalBet))
	fmt.Println("round:             ", round)
	fmt.Println("TotalBet:          ", totalBet)
	fmt.Println("NGScore:           ", NGScore)
	fmt.Println("FGScore:           ", FGScore)
	fmt.Println("intoFG:            ", intoFG)
	fmt.Println("moduleCount:       ", moduleCount)
	// fmt.Println("hallwaterlooppay:  ", hallWaterLoopPay)
	// fmt.Println("hallwaterloopbet:  ", hallWaterLoopBet)
	fmt.Println("limitPayCount:     ", limitPayCount)

	elapsed := time.Since(spendTime)
	fmt.Println("＠花費時間: ", elapsed)
	fmt.Println("------------------------")
	fmt.Println("------------------------")
}

// RiskControlRandomBet 風控隨機下注
func RiskControlRandomBet(betSlice []int, dayGroup int, limitPayOff float64, _initialWaterLoopBet float64, _initialWaterLoopPay float64) int {
	spendTime := time.Now()

	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = len(betSlice)
	var unitBet float64 = 10
	var totalBet float64 = 0
	var bet float64 = 0
	var gold float64 = 0
	var exchangeRate float64 = 0
	var NGScore float64 = 0
	var FGScore float64 = 0
	var result = game.GameResult{}
	var waterLoopBet = _initialWaterLoopBet
	var waterLoopPay = _initialWaterLoopPay
	var accumulationBet float64 = 0
	rtpmodule := 965
	var hallWaterLoopPay = []float64{}
	var hallWaterLoopBet = []float64{}

	var hallWaterLoopPayOneDay float64
	var hallWaterLoopBetOneDay float64
	var intoFG int = 0

	var moduleCount = []int{0, 0, 0}
	// 被擋做大派彩次數
	var limitPayCount int = 0

	var bar string = ""

	for time := 0; time < round; {
		// 顯示進度
		if time%(round/20) == 0 && time != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-20s]", bar)
		}

		rtpmodule = GetRtpmodule(waterLoopPay, waterLoopBet)

		if result.Status == "" || result.Status == "NORMAL" {
			bet = float64(betSlice[time])
			gold = (bet / unitBet)
			exchangeRate = 1
			if rtpmodule == 95 {
				moduleCount[0]++
			} else if rtpmodule == 965 {
				moduleCount[1]++
			} else if rtpmodule == 99 {
				moduleCount[2]++
			}

			for {
				result = game.GetNormalGameInfo(rtpmodule)
				// 需要乘上匯率
				if float64(result.TotalWin)*gold*exchangeRate > limitPayOff {
					if !GetlimitPayout(hallWaterLoopPayOneDay+float64(result.TotalWin)*gold*exchangeRate, hallWaterLoopBetOneDay, hallWaterLoopPay, hallWaterLoopBet, dayGroup) {
						break
					}
				} else {
					break
				}
				// fmt.Println("MainGame_派彩超過", LimitPayOff, ": ", float64(result.TotalWin)*gold*exchangeRate)
				// fmt.Println("MainGame_派彩超過: ", time, "--->", bet, "==>", float64(result.TotalWin)*gold*exchangeRate)
				// fmt.Println("--------------------")
				limitPayCount++
			}
			NGScore += (float64(result.TotalWin) * gold * exchangeRate)
			totalBet += (bet * exchangeRate)
			hallWaterLoopBetOneDay += (bet * exchangeRate)
			waterLoopBet += (bet * exchangeRate)
			accumulationBet += (bet * exchangeRate)
			//moduleCount[rtpmodule]++
			if result.Status == "FREE" {
				intoFG++
			}
			time++
		} else {
			for {
				result = game.GetFreeGameInfo()
				if (float64(result.TotalWin) * gold * exchangeRate) > limitPayOff {
					var tmp bool = GetlimitPayout(hallWaterLoopPayOneDay+float64(result.TotalWin)*gold*exchangeRate, hallWaterLoopBetOneDay, hallWaterLoopPay, hallWaterLoopBet, dayGroup)
					if !tmp {
						// tmp為 false: 要擋掉
						break
					}
				} else {
					break
				}
				// fmt.Println("FreeGame_派彩超過", limitPayOff, ": ", float64(result.TotalWin)*gold*exchangeRate)
				// fmt.Println("FreeGame_派彩超過: ", time, "--->", bet, "==>", float64(result.TotalWin)*gold*exchangeRate)
				// fmt.Println("============================================")
				limitPayCount++
			}
			FGScore += (float64(result.TotalWin) * gold * exchangeRate)
		}

		waterLoopPay += (float64(result.TotalWin) * gold * exchangeRate)
		hallWaterLoopPayOneDay += (float64(result.TotalWin) * gold * exchangeRate)

		if accumulationBet > 1200000 {
			waterLoopBet = _initialWaterLoopBet
			waterLoopPay = _initialWaterLoopPay
			accumulationBet = 0
		} else if accumulationBet > 800000 {
			if waterLoopPay/waterLoopBet >= LowerLimitRTP &&
				waterLoopPay/waterLoopBet <= UpperLimitRTP {
				waterLoopBet = _initialWaterLoopBet
				waterLoopPay = _initialWaterLoopPay
			}
		}
		if hallWaterLoopBetOneDay > 500000 {
			hallWaterLoopPay = append(hallWaterLoopPay, hallWaterLoopPayOneDay)
			hallWaterLoopBet = append(hallWaterLoopBet, hallWaterLoopBetOneDay)
			hallWaterLoopPayOneDay = 0
			hallWaterLoopBetOneDay = 0
		}
	}

	totalRTP := float64(NGScore+FGScore) / float64(totalBet)
	fmt.Println(totalRTP)

	fmt.Println("@基本數值")
	fmt.Println("最大派彩限制:        ", limitPayOff)
	fmt.Println("最大派彩限制天數循環: ", dayGroup)
	fmt.Println("重置押注:           ", _initialWaterLoopBet)
	fmt.Println("重置派彩:           ", _initialWaterLoopPay)
	fmt.Println("Bet:              ", bet)
	fmt.Println("RTP上限:           ", UpperLimitRTP)
	fmt.Println("RTP下限:           ", LowerLimitRTP)
	fmt.Println("總RTP:             ", totalRTP)
	fmt.Println("NG_RTP:            ", (NGScore)/(totalBet))
	fmt.Println("FG_RTP:            ", (FGScore)/(totalBet))
	fmt.Println("round:             ", round)
	fmt.Println("TotalBet:          ", totalBet)
	fmt.Println("NGScore:           ", NGScore)
	fmt.Println("FGScore:           ", FGScore)
	fmt.Println("intoFG:            ", intoFG)
	fmt.Println("moduleCount:       ", moduleCount)
	// fmt.Println("hallwaterlooppay:  ", hallWaterLoopPay)
	// fmt.Println("hallwaterloopbet:  ", hallWaterLoopBet)
	fmt.Println("limitPayCount:     ", limitPayCount)

	elapsed := time.Since(spendTime)
	fmt.Println("＠花費時間: ", elapsed)
	fmt.Println("------------------------------------------------------------")
	fmt.Println("------------------------------------------------------------")
	return limitPayCount
}

// GetRtpmodule 使用哪組rtp
func GetRtpmodule(waterlooppay float64, waterloopbet float64) int {
	rtpmodule := 965
	if waterlooppay/waterloopbet > UpperLimitRTP {
		rtpmodule = 95
	} else if waterlooppay/waterloopbet < LowerLimitRTP {
		rtpmodule = 99
	} else {
		rtpmodule = 965
	}
	return rtpmodule

}

// GetlimitPayout 是否擋最大派彩
func GetlimitPayout(HallwaterlooppayOneday float64, HallwaterloopbetOneday float64,
	Hallwaterlooppay []float64, Hallwaterloopbet []float64, dayGroup int) bool {
	var totalPayoff float64 = 0
	var totalBet float64 = 0
	for dayIndex := 1; dayIndex <= dayGroup; dayIndex++ {
		if len(Hallwaterloopbet) < dayIndex {
			break
		}
		totalPayoff += Hallwaterlooppay[len(Hallwaterlooppay)-dayIndex]
		totalBet += Hallwaterloopbet[len(Hallwaterloopbet)-dayIndex]
	}
	totalPayoff += HallwaterlooppayOneday
	totalBet += HallwaterloopbetOneday

	if float64(totalPayoff)/float64(totalBet) > HallUpperLimitRTP {
		return true
	}
	return false
}

/*========================================================================*/
/*========================================================================*/
/*========================================================================*/
// 整理資料
var pwd string
var filePath string
var fileName = "5236.csv"
var hallID []string
var hallGroup = make(map[string][][]string)

func init() {
	pwd, _ = os.Getwd()
	filePath = filepath.Join(pwd, fileName)
}

// GetData 取資料
func GetData() {
	// open file
	f, err := os.OpenFile(filePath, os.O_RDONLY, 0777)
	if err != nil {
		log.Fatal(err)
	}
	// remember to close the file at the end of the program
	defer f.Close()
	// read csv values using csv.Reader
	csvReader := csv.NewReader(f)
	// 以何種字元作分隔，預設為`,`。所以這裡可拿掉這行
	csvReader.Comma = ','

	var index int = 0
	for {
		index++
		rec, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		// do something with read line
		// fmt.Printf("%+v\n", rec)
		var repeat = true
		// index=1是title
		if index == 1 {
			continue
		}

		for index := 0; index < len(hallID); index++ {
			if rec[1] == hallID[index] {
				repeat = false
				// 下注為0表示FG，剔除
				if rec[3] != "0" {
					hallGroup[rec[1]] = append(hallGroup[rec[1]], rec)
				}
				break
			}
		}

		if repeat == true {
			hallID = append(hallID, rec[1])
			// 下注為0表示FG，剔除
			if rec[3] != "0" {
				hallGroup[rec[1]] = [][]string{rec}
			}

		}

		if index%400000 == 0 {
			fmt.Println(index)
		}

	}

	fmt.Println("")
	for index := 0; index < len(hallGroup["3820441"]); index++ {
		fmt.Println(index+1, ":", hallGroup["3820441"][index])
	}
	fmt.Println(index)
	fmt.Println(len(hallID))
	fmt.Println(hallID)
}

/*========================================================================*/
/*========================================================================*/
/*========================================================================*/
// 測試
func try() {
	hallGroup := make(map[string][][]string)
	hallGroup["1"] = [][]string{{"1", "2", "3"}}
	hallGroup["12"] = [][]string{{"11", "12", "13"}}
	hallGroup["24"] = [][]string{{"21", "22", "23"}}
	hallGroup["1"] = append(hallGroup["1"], []string{"21", "22", "23"})
	fmt.Println(hallGroup)
	fmt.Println(len(hallGroup))

	for k := range hallGroup {
		fmt.Println(k)
		fmt.Println("------")
	}

}

// HallResult 每廳的結果
type HallResult struct {
	HallID        string
	Spins         int
	TotalBet      float64
	TotalPay      float64
	AverangeRTP   float64
	RTP95         float64
	RTP965        float64
	RTP99         float64
	LimitPayCount int
}

// generateBetSlice 產生一組下注
func generateBetSlice(length int) []int {
	var bet = []int{10, 20, 30, 40, 50, 100, 200, 300, 500, 1000}
	var weight = []int{15, 13, 10, 8, 5, 3, 3, 2, 2, 2}
	var result []int
	for count := 0; count < length; count++ {
		var tmp = bet[tool.GetIndexFromWeights(weight)]
		result = append(result, tmp)
	}
	return result
}
