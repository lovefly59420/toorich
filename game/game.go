package toorich

import (
	"fmt"
	tool "tools"
)

var hit [][]int = [][]int{
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
}
var score [][]int = [][]int{
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
}
var scHit int = 0

// Reel 5個轉輪
const Reel int = 5

// Column 4,6橫排
var Column = []int{
	3, // MG
	3, // FG
}

// ScatterNumber Scatter獎圖編號
const ScatterNumber int = 10

// WildNumber WILD獎圖編號
const WildNumber int = 10

// TotalBet 玩家押注
const TotalBet int = 10

// MGINDEX mg編號
const MGINDEX int = 0

// FGINDEX fg編號
const FGINDEX int = 1

// FreeGameRound FG的局數
var FreeGameRound int = 10

// NormalGameReel965 NG轉輪表
var NormalGameReel965 = [][]int{
	// 965
	{1, 7, 8, 2, 6, 1, 8, 6, 7, 3, 8, 9, 4, 6, 5, 8, 2, 6, 3, 8, 4, 5, 8, 7, 9, 8, 1, 5, 3, 7, 8, 6, 9, 7, 8, 3, 9, 5, 4, 6, 7, 10, 9, 7, 5, 9, 7, 3, 9, 5, 4, 9, 6, 1, 9, 6, 2, 9, 3, 5},
	{5, 4, 9, 3, 8, 7, 5, 9, 8, 7, 5, 9, 6, 7, 5, 2, 4, 5, 7, 4, 5, 6, 4, 9, 7, 6, 8, 5, 1, 6, 3, 7, 2, 6, 7, 3, 8, 6, 4, 8, 9, 10, 8, 9, 2, 6, 8, 3, 9, 8, 4, 6, 7, 1, 6, 7, 2, 8, 9, 7},
	{1, 8, 4, 6, 7, 4, 3, 8, 5, 10, 8, 5, 10, 8, 9, 10, 6, 5, 3, 2, 5, 4, 2, 6, 8, 4, 9, 6, 3, 2, 7, 5, 6, 7, 2, 3, 7, 5, 4, 7, 6, 10, 9, 7, 2, 6, 8, 3, 7, 6, 4, 9, 7, 1, 6, 9, 2, 8, 7, 9},
	{1, 7, 5, 4, 8, 1, 3, 4, 8, 3, 5, 8, 4, 3, 9, 10, 5, 8, 3, 9, 5, 4, 8, 2, 3, 9, 7, 10, 8, 2, 4, 7, 1, 6, 5, 3, 7, 5, 4, 9, 6, 10, 7, 9, 2, 5, 6, 3, 7, 5, 4, 2, 9, 1, 7, 5, 2, 9, 8, 7},
	{3, 8, 1, 4, 8, 6, 2, 5, 6, 4, 3, 7, 1, 5, 3, 2, 7, 5, 3, 9, 2, 4, 3, 6, 2, 1, 8, 3, 9, 6, 4, 2, 8, 6, 4, 3, 9, 7, 4, 6, 9, 10, 8, 7, 2, 9, 7, 3, 9, 2, 4, 9, 7, 1, 9, 8, 2, 7, 8, 9},
}

// NormalGameReel95 NG轉輪表
var NormalGameReel95 = [][]int{
	// 95
	{1, 7, 8, 2, 6, 1, 8, 6, 7, 3, 8, 9, 4, 6, 5, 8, 2, 6, 3, 8, 4, 5, 8, 7, 9, 8, 1, 5, 3, 7, 8, 6, 9, 7, 8, 3, 9, 5, 4, 6, 7, 10, 9, 7, 5, 6, 7, 3, 9, 5, 4, 9, 6, 1, 9, 6, 2, 9, 3, 5},
	{5, 4, 9, 6, 8, 7, 5, 9, 8, 7, 5, 9, 6, 7, 5, 2, 4, 5, 7, 4, 5, 6, 4, 9, 7, 6, 8, 5, 1, 6, 3, 7, 2, 6, 7, 3, 8, 6, 4, 8, 9, 10, 8, 9, 2, 6, 8, 3, 9, 8, 4, 6, 7, 1, 6, 7, 2, 8, 9, 7},
	{1, 8, 4, 6, 7, 4, 3, 8, 5, 10, 8, 5, 10, 8, 9, 10, 6, 5, 3, 2, 5, 4, 2, 6, 8, 4, 9, 6, 3, 2, 7, 5, 6, 7, 2, 3, 7, 5, 4, 7, 6, 10, 9, 7, 2, 6, 8, 3, 7, 6, 4, 9, 7, 1, 6, 9, 2, 8, 7, 9},
	{1, 7, 5, 4, 8, 1, 3, 4, 8, 3, 5, 8, 4, 3, 9, 10, 5, 8, 3, 9, 5, 4, 8, 2, 3, 9, 7, 10, 8, 2, 4, 7, 1, 6, 5, 3, 7, 5, 4, 9, 6, 10, 7, 9, 2, 5, 6, 3, 7, 5, 4, 2, 9, 1, 7, 5, 2, 9, 8, 7},
	{3, 8, 1, 4, 8, 6, 2, 5, 6, 4, 3, 7, 1, 5, 3, 2, 7, 5, 3, 9, 2, 4, 3, 6, 2, 1, 8, 3, 9, 6, 4, 2, 8, 6, 4, 3, 9, 7, 4, 6, 9, 10, 8, 7, 2, 9, 7, 3, 9, 2, 4, 9, 7, 1, 9, 8, 2, 7, 8, 9},
}

// NormalGameReel99 NG轉輪表
var NormalGameReel99 = [][]int{
	// 99
	{1, 7, 8, 2, 6, 1, 8, 6, 7, 3, 8, 9, 4, 6, 5, 8, 2, 6, 3, 8, 4, 5, 8, 7, 9, 8, 1, 5, 3, 7, 8, 6, 9, 7, 8, 3, 9, 5, 4, 6, 7, 10, 9, 7, 5, 9, 7, 3, 9, 5, 4, 9, 6, 1, 9, 6, 2, 9, 3, 5},
	{5, 4, 9, 3, 8, 7, 5, 9, 8, 4, 5, 9, 6, 7, 5, 2, 4, 5, 7, 4, 5, 6, 4, 9, 7, 4, 8, 5, 1, 6, 3, 7, 2, 6, 7, 3, 8, 6, 4, 8, 9, 10, 8, 9, 2, 6, 8, 3, 9, 8, 4, 6, 7, 1, 6, 7, 2, 8, 9, 7},
	{1, 8, 4, 6, 7, 4, 3, 8, 5, 10, 8, 5, 10, 8, 9, 10, 6, 5, 3, 2, 5, 4, 2, 6, 8, 4, 9, 6, 3, 2, 7, 5, 6, 7, 2, 3, 7, 5, 4, 7, 6, 10, 9, 7, 2, 6, 8, 3, 7, 6, 4, 9, 7, 1, 6, 9, 2, 8, 7, 9},
	{1, 7, 5, 4, 8, 1, 3, 4, 8, 3, 5, 8, 4, 3, 9, 10, 7, 8, 3, 9, 5, 4, 8, 2, 3, 9, 7, 10, 8, 2, 4, 7, 1, 6, 5, 3, 7, 5, 4, 9, 6, 10, 7, 9, 2, 5, 6, 3, 7, 5, 4, 2, 9, 1, 7, 5, 2, 9, 8, 7},
	{3, 8, 1, 4, 8, 6, 2, 5, 6, 4, 3, 7, 1, 5, 3, 2, 7, 5, 3, 9, 2, 4, 3, 6, 2, 1, 8, 3, 9, 6, 4, 2, 8, 6, 4, 3, 9, 7, 4, 6, 9, 10, 8, 7, 2, 9, 7, 3, 9, 2, 4, 9, 7, 1, 9, 8, 2, 7, 8, 9},
}

// FreeGameReel FG轉輪表
var FreeGameReel = [][][]int{
	// M1
	[][]int{
		{1, 7, 8, 5, 6, 1, 8, 5, 7, 8, 6, 1, 9, 6, 5, 1, 7, 6, 3, 8, 5, 4, 1, 7, 5, 8, 9, 3, 5, 7, 8, 6, 5, 7, 1, 3, 9, 5, 4, 6, 7, 10, 9, 7, 5, 9, 7, 3, 9, 5, 4, 9, 6, 1, 9, 6, 2, 9, 3, 5},
		{4, 9, 5, 6, 8, 7, 5, 9, 8, 7, 5, 9, 6, 7, 5, 2, 6, 5, 7, 9, 5, 4, 6, 9, 7, 6, 8, 7, 9, 6, 8, 7, 9, 6, 7, 3, 8, 6, 4, 8, 9, 10, 8, 9, 2, 6, 8, 3, 9, 8, 4, 6, 7, 1, 6, 7, 2, 8, 9, 7},
		{1, 8, 6, 5, 7, 4, 3, 8, 5, 1, 8, 5, 10, 8, 9, 2, 6, 5, 3, 9, 5, 4, 1, 6, 8, 5, 9, 6, 3, 2, 7, 1, 6, 7, 2, 3, 7, 5, 4, 7, 6, 10, 9, 7, 2, 6, 8, 3, 7, 6, 4, 9, 7, 4, 6, 9, 2, 8, 7, 9},
		{1, 7, 5, 6, 8, 1, 3, 5, 8, 4, 5, 8, 10, 7, 9, 1, 5, 8, 1, 9, 5, 4, 8, 2, 6, 9, 7, 10, 8, 9, 6, 7, 1, 6, 5, 3, 7, 5, 1, 9, 6, 10, 7, 9, 2, 5, 6, 3, 7, 5, 4, 2, 9, 1, 7, 5, 2, 9, 8, 7},
		{3, 8, 5, 7, 8, 6, 3, 5, 6, 4, 9, 7, 10, 5, 6, 2, 7, 5, 3, 9, 5, 4, 3, 6, 7, 9, 8, 10, 9, 6, 4, 9, 8, 6, 4, 3, 9, 7, 4, 6, 9, 10, 8, 7, 2, 9, 7, 3, 9, 2, 4, 9, 7, 1, 9, 8, 2, 7, 8, 9},
	},
	// M2
	[][]int{
		{2, 7, 8, 5, 6, 2, 8, 5, 7, 8, 6, 2, 9, 6, 5, 2, 7, 6, 3, 8, 5, 4, 2, 7, 5, 8, 9, 3, 5, 2, 8, 6, 5, 7, 2, 3, 9, 5, 4, 6, 7, 10, 9, 7, 5, 9, 7, 3, 9, 5, 4, 9, 6, 2, 9, 6, 1, 9, 3, 5},
		{4, 9, 5, 6, 8, 7, 5, 9, 8, 7, 5, 9, 6, 7, 5, 1, 6, 5, 7, 9, 5, 4, 6, 9, 7, 6, 8, 7, 9, 6, 8, 7, 9, 6, 7, 3, 8, 6, 4, 8, 9, 10, 8, 9, 1, 6, 8, 3, 9, 8, 4, 6, 7, 2, 6, 7, 1, 8, 9, 7},
		{2, 8, 6, 5, 2, 4, 3, 8, 5, 2, 8, 5, 10, 8, 9, 1, 6, 5, 3, 9, 5, 4, 2, 6, 8, 5, 2, 6, 3, 1, 7, 2, 6, 7, 1, 3, 7, 5, 4, 7, 6, 10, 9, 7, 1, 6, 8, 3, 7, 6, 4, 9, 7, 4, 6, 9, 1, 8, 7, 9},
		{2, 7, 5, 6, 8, 2, 3, 5, 2, 4, 5, 8, 10, 7, 9, 2, 5, 8, 2, 9, 5, 4, 8, 1, 6, 9, 7, 10, 8, 9, 6, 7, 2, 6, 5, 3, 7, 5, 2, 9, 6, 10, 7, 9, 1, 5, 6, 3, 7, 5, 4, 1, 9, 2, 7, 5, 1, 9, 8, 7},
		{3, 8, 5, 7, 8, 6, 3, 5, 6, 4, 9, 7, 10, 5, 6, 1, 7, 5, 3, 9, 5, 4, 3, 6, 7, 9, 8, 10, 9, 6, 4, 9, 8, 6, 4, 3, 9, 7, 4, 6, 9, 10, 8, 7, 1, 9, 7, 3, 9, 1, 4, 9, 7, 2, 9, 8, 1, 7, 8, 9},
	},

	// M3
	[][]int{
		{3, 7, 8, 5, 6, 3, 8, 5, 7, 8, 6, 3, 9, 6, 5, 3, 7, 6, 2, 8, 5, 4, 3, 7, 5, 8, 9, 2, 5, 3, 8, 6, 5, 7, 3, 2, 9, 5, 4, 6, 7, 10, 9, 7, 5, 9, 7, 2, 9, 5, 4, 9, 6, 3, 9, 6, 1, 9, 2, 5},
		{4, 9, 5, 6, 8, 7, 5, 9, 8, 7, 5, 9, 6, 7, 3, 1, 6, 5, 7, 9, 5, 4, 6, 9, 7, 2, 8, 7, 9, 6, 8, 3, 9, 6, 7, 2, 8, 6, 4, 8, 9, 10, 8, 9, 1, 6, 8, 2, 9, 8, 4, 6, 7, 3, 6, 7, 1, 8, 9, 7},
		{3, 8, 6, 5, 3, 4, 2, 8, 5, 3, 8, 5, 10, 8, 9, 1, 6, 5, 2, 9, 5, 4, 3, 6, 8, 5, 3, 6, 2, 1, 7, 3, 6, 7, 1, 2, 7, 5, 4, 7, 6, 10, 9, 7, 1, 6, 8, 2, 7, 6, 4, 9, 7, 4, 6, 9, 1, 8, 7, 9},
		{3, 7, 5, 6, 8, 3, 2, 5, 3, 4, 5, 8, 10, 7, 9, 3, 5, 8, 3, 9, 5, 4, 8, 1, 6, 9, 7, 10, 8, 9, 6, 7, 3, 6, 5, 2, 7, 5, 3, 9, 6, 10, 7, 9, 1, 5, 6, 2, 7, 5, 4, 1, 9, 3, 7, 5, 1, 9, 8, 7},
		{2, 8, 5, 7, 8, 6, 2, 5, 6, 4, 9, 7, 10, 5, 6, 1, 7, 5, 2, 9, 5, 4, 2, 6, 7, 9, 8, 10, 9, 6, 4, 9, 8, 6, 4, 2, 9, 7, 4, 6, 9, 10, 8, 7, 1, 9, 7, 2, 9, 1, 4, 9, 7, 3, 9, 8, 1, 7, 8, 9},
	},

	// M4
	[][]int{
		{4, 7, 8, 5, 6, 4, 8, 5, 7, 8, 6, 4, 9, 6, 5, 4, 7, 6, 2, 8, 5, 3, 4, 7, 5, 8, 9, 2, 5, 4, 8, 6, 5, 7, 4, 2, 9, 5, 3, 6, 7, 10, 9, 7, 5, 9, 7, 2, 9, 5, 3, 9, 6, 4, 9, 6, 1, 9, 2, 5},
		{3, 9, 5, 6, 8, 7, 5, 9, 8, 7, 5, 9, 6, 7, 4, 1, 6, 5, 7, 9, 5, 3, 6, 9, 7, 2, 8, 7, 9, 6, 8, 4, 9, 6, 7, 2, 8, 6, 3, 8, 9, 10, 8, 9, 1, 6, 8, 2, 9, 8, 3, 6, 7, 4, 6, 7, 1, 8, 9, 7},
		{4, 8, 6, 5, 4, 3, 2, 8, 5, 4, 8, 5, 10, 8, 9, 1, 6, 5, 2, 9, 5, 3, 4, 6, 8, 5, 4, 6, 2, 1, 7, 4, 6, 7, 1, 2, 7, 5, 3, 7, 6, 10, 9, 7, 1, 6, 8, 2, 7, 6, 3, 9, 7, 3, 6, 9, 1, 8, 7, 9},
		{4, 7, 5, 6, 8, 4, 2, 5, 4, 3, 5, 8, 10, 7, 9, 4, 5, 8, 4, 9, 5, 3, 8, 1, 6, 9, 7, 10, 8, 9, 6, 7, 4, 6, 5, 2, 7, 5, 4, 9, 6, 10, 7, 9, 1, 5, 6, 2, 7, 5, 3, 1, 9, 4, 7, 5, 1, 9, 8, 7},
		{2, 8, 5, 7, 8, 6, 2, 5, 6, 3, 9, 7, 10, 5, 6, 1, 7, 5, 2, 9, 5, 3, 2, 6, 7, 9, 8, 10, 9, 6, 3, 9, 8, 6, 3, 2, 9, 7, 3, 6, 9, 10, 8, 7, 1, 9, 7, 2, 9, 1, 3, 9, 7, 4, 9, 8, 1, 7, 8, 9},
	},

	// A
	[][]int{
		{5, 7, 8, 4, 6, 5, 8, 4, 7, 8, 6, 5, 9, 6, 8, 5, 7, 6, 2, 8, 6, 4, 5, 7, 4, 8, 9, 2, 3, 5, 8, 6, 3, 7, 5, 2, 9, 3, 4, 6, 7, 10, 9, 7, 3, 9, 7, 2, 9, 3, 4, 9, 6, 5, 9, 6, 1, 9, 2, 3},
		{4, 9, 3, 6, 5, 7, 3, 9, 8, 7, 5, 9, 6, 7, 5, 1, 6, 5, 7, 9, 5, 4, 6, 9, 7, 2, 8, 7, 5, 6, 8, 5, 9, 6, 7, 2, 8, 6, 4, 8, 9, 10, 8, 9, 1, 6, 8, 2, 9, 8, 4, 6, 7, 5, 6, 7, 1, 8, 9, 7},
		{5, 8, 6, 3, 5, 4, 2, 8, 3, 5, 8, 3, 10, 8, 9, 1, 6, 5, 2, 9, 3, 4, 5, 6, 8, 3, 5, 6, 2, 1, 7, 5, 6, 7, 1, 2, 7, 3, 4, 7, 6, 10, 9, 7, 1, 6, 8, 2, 7, 6, 4, 9, 7, 4, 5, 9, 1, 8, 7, 9},
		{5, 7, 3, 6, 8, 5, 2, 3, 5, 4, 3, 8, 10, 7, 9, 5, 3, 8, 5, 9, 4, 5, 8, 1, 6, 9, 7, 10, 8, 9, 6, 7, 5, 6, 3, 2, 7, 3, 5, 9, 6, 10, 7, 9, 1, 3, 6, 2, 7, 3, 4, 1, 9, 5, 7, 3, 1, 9, 8, 7},
		{2, 8, 3, 7, 5, 6, 2, 3, 6, 4, 9, 7, 10, 3, 6, 1, 7, 3, 2, 9, 3, 4, 2, 6, 7, 9, 8, 10, 9, 6, 4, 9, 5, 6, 4, 2, 9, 7, 4, 6, 9, 10, 8, 7, 1, 9, 7, 2, 9, 1, 4, 9, 7, 5, 9, 8, 1, 7, 8, 9},
	},

	// K
	[][]int{
		{6, 7, 8, 4, 5, 6, 8, 4, 7, 8, 5, 6, 9, 5, 8, 6, 7, 5, 2, 8, 5, 4, 6, 7, 4, 8, 9, 2, 3, 6, 8, 5, 3, 7, 6, 2, 9, 3, 4, 5, 7, 10, 9, 7, 3, 9, 7, 2, 9, 3, 4, 9, 5, 6, 9, 5, 1, 9, 2, 3},
		{4, 9, 3, 5, 6, 7, 3, 9, 8, 7, 6, 9, 5, 7, 6, 1, 5, 6, 7, 9, 6, 4, 5, 9, 7, 2, 8, 7, 6, 5, 8, 6, 9, 5, 7, 2, 8, 5, 4, 8, 9, 10, 8, 9, 1, 5, 8, 2, 9, 8, 4, 5, 7, 6, 5, 7, 1, 8, 9, 7},
		{6, 8, 5, 3, 6, 4, 2, 8, 3, 6, 8, 3, 10, 8, 9, 1, 5, 6, 2, 9, 3, 4, 6, 5, 8, 3, 6, 5, 2, 1, 7, 6, 5, 7, 1, 2, 7, 3, 4, 7, 5, 10, 9, 7, 1, 5, 8, 2, 7, 5, 4, 9, 7, 4, 6, 9, 1, 8, 7, 9},
		{6, 7, 3, 5, 8, 6, 2, 3, 6, 4, 3, 8, 10, 7, 9, 6, 3, 8, 6, 9, 4, 6, 8, 1, 5, 9, 7, 10, 8, 9, 5, 7, 6, 5, 3, 2, 7, 3, 6, 9, 5, 10, 7, 9, 1, 3, 5, 2, 7, 3, 4, 1, 9, 6, 7, 3, 1, 9, 8, 7},
		{2, 8, 3, 7, 6, 5, 2, 3, 5, 4, 9, 7, 10, 3, 5, 1, 7, 3, 2, 9, 3, 4, 2, 5, 7, 9, 8, 10, 9, 5, 4, 9, 6, 5, 4, 2, 9, 7, 4, 5, 9, 10, 8, 7, 1, 9, 7, 2, 9, 1, 4, 9, 7, 6, 9, 8, 1, 7, 8, 9},
	},

	// Q
	[][]int{
		{7, 5, 8, 4, 6, 7, 8, 4, 5, 8, 6, 7, 9, 6, 8, 7, 5, 6, 2, 8, 6, 4, 7, 5, 4, 8, 9, 2, 3, 7, 8, 6, 3, 5, 7, 2, 9, 3, 4, 6, 5, 10, 9, 5, 3, 9, 5, 2, 9, 3, 4, 9, 6, 7, 9, 6, 1, 9, 2, 3},
		{4, 9, 3, 6, 7, 5, 3, 9, 8, 5, 7, 9, 6, 5, 7, 1, 6, 7, 5, 9, 7, 4, 6, 9, 5, 2, 8, 5, 7, 6, 8, 7, 9, 6, 5, 2, 8, 6, 4, 8, 9, 10, 8, 9, 1, 6, 8, 2, 9, 8, 4, 6, 5, 7, 6, 5, 1, 8, 9, 5},
		{7, 8, 6, 3, 7, 4, 2, 8, 3, 7, 8, 3, 10, 8, 9, 1, 6, 7, 2, 9, 3, 4, 7, 6, 8, 3, 7, 6, 2, 1, 5, 7, 6, 5, 1, 2, 5, 3, 4, 5, 6, 10, 9, 5, 1, 6, 8, 2, 5, 6, 4, 9, 5, 4, 7, 9, 1, 8, 5, 9},
		{7, 5, 3, 6, 8, 7, 2, 3, 7, 4, 3, 8, 10, 5, 9, 7, 3, 8, 7, 9, 4, 7, 8, 1, 6, 9, 5, 10, 8, 9, 6, 5, 7, 6, 3, 2, 5, 3, 7, 9, 6, 10, 5, 9, 1, 3, 6, 2, 5, 3, 4, 1, 9, 7, 5, 3, 1, 9, 8, 5},
		{2, 8, 3, 5, 7, 6, 2, 3, 6, 4, 9, 5, 10, 3, 6, 1, 5, 3, 2, 9, 3, 4, 2, 6, 5, 9, 8, 10, 9, 6, 4, 9, 7, 6, 4, 2, 9, 5, 4, 6, 9, 10, 8, 5, 1, 9, 5, 2, 9, 1, 4, 9, 5, 7, 9, 8, 1, 5, 8, 9},
	},

	// J
	[][]int{
		{8, 5, 7, 4, 6, 8, 7, 4, 5, 7, 6, 8, 9, 6, 7, 8, 5, 6, 2, 7, 6, 4, 8, 5, 4, 7, 9, 2, 3, 8, 7, 6, 3, 5, 8, 2, 9, 3, 4, 6, 5, 10, 9, 5, 3, 9, 5, 2, 9, 3, 4, 9, 6, 8, 9, 6, 1, 9, 2, 3},
		{4, 9, 3, 6, 8, 5, 3, 9, 7, 5, 8, 9, 6, 5, 8, 1, 6, 8, 5, 9, 8, 4, 6, 9, 5, 2, 7, 5, 8, 6, 7, 8, 9, 6, 5, 2, 7, 6, 4, 7, 9, 10, 7, 9, 1, 6, 7, 2, 9, 7, 4, 6, 5, 8, 6, 5, 1, 7, 9, 5},
		{8, 7, 6, 3, 8, 4, 2, 7, 3, 8, 7, 3, 10, 7, 9, 1, 6, 8, 2, 9, 3, 4, 8, 6, 7, 3, 8, 6, 2, 1, 5, 8, 6, 5, 1, 2, 5, 3, 4, 5, 6, 10, 9, 5, 1, 6, 7, 2, 5, 6, 4, 9, 5, 4, 8, 9, 1, 7, 5, 9},
		{8, 5, 3, 6, 7, 8, 2, 3, 8, 4, 3, 7, 10, 5, 9, 8, 3, 7, 8, 9, 4, 8, 7, 1, 6, 9, 5, 10, 7, 9, 6, 5, 8, 6, 3, 2, 5, 3, 8, 9, 6, 10, 5, 9, 1, 3, 6, 2, 5, 3, 4, 1, 9, 8, 5, 3, 1, 9, 7, 5},
		{2, 7, 3, 5, 8, 6, 2, 3, 6, 4, 9, 5, 10, 3, 6, 1, 5, 3, 2, 9, 3, 4, 2, 6, 5, 9, 7, 10, 9, 6, 4, 9, 8, 6, 4, 2, 9, 5, 4, 6, 9, 10, 7, 5, 1, 9, 5, 2, 9, 1, 4, 9, 5, 8, 9, 7, 1, 5, 7, 9},
	},

	// TE
	[][]int{
		{9, 5, 8, 4, 6, 9, 8, 4, 5, 8, 6, 9, 7, 6, 8, 9, 5, 6, 2, 8, 6, 4, 9, 5, 4, 8, 7, 2, 3, 9, 8, 6, 3, 5, 9, 2, 7, 3, 4, 6, 5, 10, 7, 5, 3, 7, 5, 2, 7, 3, 4, 7, 6, 9, 7, 6, 1, 7, 2, 3},
		{4, 7, 3, 6, 9, 5, 3, 7, 8, 5, 9, 7, 6, 5, 9, 1, 6, 9, 5, 7, 9, 4, 6, 7, 5, 2, 8, 5, 9, 6, 8, 9, 7, 6, 5, 2, 8, 6, 4, 8, 7, 10, 8, 7, 1, 6, 8, 2, 7, 8, 4, 6, 5, 9, 6, 5, 1, 8, 7, 5},
		{9, 8, 6, 3, 9, 4, 2, 8, 3, 9, 8, 3, 10, 8, 7, 1, 6, 9, 2, 7, 3, 4, 9, 6, 8, 3, 9, 6, 2, 1, 5, 9, 6, 5, 1, 2, 5, 3, 4, 5, 6, 10, 7, 5, 1, 6, 8, 2, 5, 6, 4, 7, 5, 4, 9, 7, 1, 8, 5, 7},
		{9, 5, 3, 6, 8, 9, 2, 3, 9, 4, 3, 8, 10, 5, 7, 9, 3, 8, 9, 7, 4, 9, 8, 1, 6, 7, 5, 10, 8, 7, 6, 5, 9, 6, 3, 2, 5, 3, 9, 7, 6, 10, 5, 7, 1, 3, 6, 2, 5, 3, 4, 1, 7, 9, 5, 3, 1, 7, 8, 5},
		{2, 8, 3, 5, 9, 6, 2, 3, 6, 4, 7, 5, 10, 3, 6, 1, 5, 3, 2, 7, 3, 4, 2, 6, 5, 7, 8, 10, 7, 6, 4, 7, 9, 6, 4, 2, 7, 5, 4, 6, 7, 10, 8, 5, 1, 7, 5, 2, 7, 1, 4, 7, 5, 9, 7, 8, 1, 5, 8, 7},
	},
}

// goldSymbolWeig 金色獎圖權重
var goldSymbolWeig = []int{
	0,          // 空
	2, 3, 2, 2, // M1 ~ M4
	3, 3, 3, 3, 2, // A ~ TE
}

// PayTable 賠率表
var payTable = [][]int{
	/* 0  */ /* 無  */ {0, 0, 0, 0, 0, 0},
	/* 1  */ /* M1  */ {0, 0, 10, 100, 500, 2000},
	/* 2  */ /* M2  */ {0, 0, 5, 40, 250, 750},
	/* 3  */ /* M3  */ {0, 0, 5, 30, 75, 500},
	/* 4  */ /* M4  */ {0, 0, 5, 30, 75, 500},
	/* 5  */ /* A   */ {0, 0, 0, 5, 30, 120},
	/* 6  */ /* K   */ {0, 0, 0, 5, 30, 120},
	/* 7  */ /* Q   */ {0, 0, 0, 5, 20, 80},
	/* 8  */ /* J   */ {0, 0, 0, 5, 20, 80},
	/* 9  */ /* TE  */ {0, 0, 0, 5, 20, 80},
	/* 10 */ /* WW  */ {0, 0, 10, 100, 1000, 5000},
	/* 11 */ /* SC  */ {0, 0, 0, 2, 20, 200},
}

// mgLinePos 連線的位置
// 0 0 0 0 0
var mgLinePos = [][]int{
	// 1 ~ 5
	{1, 1, 1, 1, 1},
	{0, 0, 0, 0, 0}, {2, 2, 2, 2, 2}, {0, 1, 2, 1, 0}, {2, 1, 0, 1, 2},
	// 6 ~ 10
	{1, 0, 0, 0, 1}, {1, 2, 2, 2, 1}, {0, 0, 1, 2, 2}, {2, 2, 1, 0, 0}, {1, 2, 1, 0, 1},
}

// fgLinePos 連線的位置
// 0 0 0 0 0
var fgLinePos = [][]int{
	// 1 ~ 5
	{1, 1, 1, 1, 1},
	{0, 0, 0, 0, 0}, {2, 2, 2, 2, 2}, {0, 1, 2, 1, 0}, {2, 1, 0, 1, 2},
	// 6 ~ 10
	{1, 0, 0, 0, 1}, {1, 2, 2, 2, 1}, {0, 0, 1, 2, 2}, {2, 2, 1, 0, 0}, {1, 2, 1, 0, 1},
}

func aaa() {
	fmt.Println("XXXXX")
}

// GetNormalGameInfo 取得 Normal Game 遊戲結果
func GetNormalGameInfo(rtp int) GameResult {
	var result = GameResult{}
	var gameInfo = []GameInfo{}
	var tmp = GameInfo{}

	rngReult := GetOneRNGforEveryReelForNormalGame(rtp)
	reelResult := GetNotSortSymbolsForNormalGame(rngReult, rtp)

	tmp.Reel = reelResult
	tmp.WinLineInfo = GetWinLineInfo(tmp.Reel, 1, mgLinePos, 0)
	tmp.FGWinInfo = GetScatterWinInfo(tmp.Reel, 1)
	gameInfo = append(gameInfo, tmp)

	// // debug
	// fmt.Println("--------debug_03--------")
	// fmt.Println("reel:     ", tmp.Reel)
	// fmt.Println("-------------------------")
	// fmt.Println("===================================")

	result.TotalWin = GetTotalWin(gameInfo)
	result.Status = "NORMAL"

	if tmp.FGWinInfo.WinPoint > 0 {
		result.Status = "FREE"
	}

	result.Info = gameInfo

	return result
}

// GetFixNormalGameInfo 取得 Normal Game 遊戲結果
func GetFixNormalGameInfo(gold int) GameResult {
	var result = GameResult{}
	var gameInfo = []GameInfo{}
	var tmp = GameInfo{}
	symbol := [][]int{
		// NG -> FG
		//{2, 2, 3}, {1, 2, 3}, {1, 2, 3}, {1, 2, 3}, {2, 2, 3},
		// 連線組合包括WILD
		// {2, 2, 3}, {2, 6, 10}, {2, 5, 2}, {2, 10, 4}, {2, 6, 7},
		{2, 2, 3}, {2, 6, 10}, {2, 5, 10}, {5, 10, 4}, {6, 6, 7},

		//驗算
		//{4, 2, 3}, {4, 2, 3}, {4, 2, 3}, {4, 2, 3}, {4, 2, 3},
		//{5, 6, 7}, {5, 6, 7}, {5, 6, 7}, {5, 6, 7}, {5, 6, 7},
		//{8, 9, 10}, {8, 9, 10}, {8, 9, 10}, {8, 9, 10}, {8, 9, 10},
		//{11, 12, 2}, {11, 12, 2}, {11, 12, 3}, {11, 12, 4}, {11, 12, 5},
		//{2, 3, 4}, {1, 2, 3}, {1, 2, 3}, {1, 2, 3}, {0, 0, 0},
		//檢查第一轉輪1,3位置相同獎圖是否會重複計分
		//{2, 3, 2}, {2, 3, 2}, {2, 3, 2}, {2, 3, 2}, {2, 3, 2},
		//檢查第一轉輪2,3位置相同獎圖是否會重複計分
		//{2, 3, 3}, {2, 3, 3}, {2, 3, 3}, {2, 3, 3}, {2, 3, 3},
		//{2, 2, 3}, {2, 5, 3}, {2, 5, 3}, {2, 5, 3}, {2, 5, 3},
		//{2, 3, 4}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0},
	}
	reelResult := symbol

	tmp.Reel = reelResult

	tmp.WinLineInfo = GetWinLineInfo(tmp.Reel, gold, mgLinePos, 0)
	tmp.FGWinInfo = GetScatterWinInfo(tmp.Reel, gold)
	gameInfo = append(gameInfo, tmp)

	result.TotalWin = GetTotalWin(gameInfo)
	result.Status = "NORMAL"

	if tmp.FGWinInfo.WinPoint > 0 {
		result.Status = "FREE"
	}

	result.Info = gameInfo

	return result
}

// GetFreeGameInfo 取得 Free Game 遊戲結果
func GetFreeGameInfo() GameResult {
	var result = GameResult{}
	var gameInfo = []GameInfo{}
	var fs int = FreeGameRound
	var totalRound int = 0
	var trigger int = 0
	var goldSymbol int = tool.GetIndexFromWeights(goldSymbolWeig)

	for ; fs > 0; fs-- {
		tmp := GameInfo{}

		rngReult := GetOneRNGforEveryReelForFreeGame(goldSymbol - 1)
		reelResult := GetNotSortSymbolsForFreeGame(rngReult, goldSymbol-1)

		tmp.Reel = reelResult
		// tmp.Reel = [][]int{{1, 2, 3}, {1, 5, 6}, {1, 9, 10}, {4, 9, 10}, {1, 9, 10}}
		tmp.WinLineInfo = GetWinLineInfo(tmp.Reel, 1, fgLinePos, goldSymbol)
		tmp.GoldWinLineInfo = GetGoldWinLineInfo(tmp.Reel, 1, fgLinePos, goldSymbol)
		tmp.FGWinInfo = GetScatterWinInfo(tmp.Reel, 1)
		gameInfo = append(gameInfo, tmp)

		// // debug
		// fmt.Println("--------debug_10--------")
		// fmt.Println("totalRound: ", totalRound+1)
		// fmt.Println("trigger:    ", trigger)
		// fmt.Println("reel:       ", tmp.Reel)
		// fmt.Println("-------------------------")
		// fmt.Println("===================================")

		// Retrigger
		if tmp.FGWinInfo.WinPoint > 0 && trigger < 19 {
			fs += FreeGameRound
			trigger++
		}
		totalRound++
	}

	result.TotalWin = GetTotalWin(gameInfo)
	result.Info = gameInfo
	result.Status = "NORMAL"

	return result
}

// GetAllNormalGameInfo 取得 Normal Game 遊戲結果 _____窮舉
func GetAllNormalGameInfo() {
	round := 0
	var totalWin int = 0
	var normalWin int = 0
	var rtp int = 965
	// var reelIndex = GetReelIndex(mgFeatureWeig)

	for ss := 0; ss < len(NormalGameReel965[0]); ss++ {
		for sss := 0; sss < len(NormalGameReel965[1]); sss++ {
			for ssss := 0; ssss < len(NormalGameReel965[2]); ssss++ {
				for sssss := 0; sssss < len(NormalGameReel965[3]); sssss++ {
					for ssssss := 0; ssssss < len(NormalGameReel965[4]); ssssss++ {
						round++
						var result = GameResult{}
						var gameInfo = []GameInfo{}
						var tmp = GameInfo{}

						rngReult := []int{ss, sss, ssss, sssss, ssssss}
						// rngReult = []int{26, 26, 25, 3, 3}
						reelResult := GetNotSortSymbolsForNormalGame(rngReult, rtp)
						// reelResult := [][]int{
						// 	[]int{1, 2, 3},
						// 	[]int{1, 4, 5},
						// 	[]int{1, 6, 7},
						// 	[]int{10, 6, 7},
						// 	[]int{9, 6, 7},
						// }
						tmp.Reel = reelResult

						tmp.WinLineInfo = GetWinLineInfo(tmp.Reel, 1, mgLinePos, 0)
						tmp.FGWinInfo = GetScatterWinInfo(tmp.Reel, 1)
						gameInfo = append(gameInfo, tmp)

						result.TotalWin = GetTotalWin(gameInfo)
						result.Info = gameInfo
						result.Status = "NORMAL"

						//fmt.Println(result.TotalWin)
						totalWin += result.TotalWin
						normalWin += result.TotalWin
						normalWin -= result.Info[0].FGWinInfo.WinPoint
						// if round > 1177599 {
						// 	fmt.Println(round)
						// }

						if result.TotalWin > 0 {
							for ii := 0; ii < len(tmp.WinLineInfo); ii++ {
								hit[tmp.WinLineInfo[ii].Count-2][tmp.WinLineInfo[ii].Symbol]++
								score[tmp.WinLineInfo[ii].Count-2][tmp.WinLineInfo[ii].Symbol] += tmp.WinLineInfo[ii].WinPoint
							}

						}

						if tmp.FGWinInfo.WinPoint > 0 {
							scHit++
						}

					}
				}
			}
		}
		fmt.Println(ss)
	}
	fmt.Println("round:    ", round)
	fmt.Println("SCATTER:  ", scHit)
	fmt.Println("Trigger:  ", float32(scHit)/float32(round))
	fmt.Println("rtp:      ", float32(totalWin)/float32(round)/float32(1))
	fmt.Println("sc_rtp:   ", float32(totalWin-normalWin)/float32(round)/float32(10))
	fmt.Println("normal_rtp:", float32(normalWin)/float32(round)/float32(1))
	fmt.Println("TotalWin: ", totalWin)
	fmt.Println("NormalWin: ", normalWin)
	fmt.Println("--------------------")

	fmt.Println("-----HIT-----")
	for ss := 0; ss < len(hit[0]); ss++ {
		// fmt.Println("4連線: ", ss, float32(Hit[0][ss])/float32(round))
		fmt.Println("2連線: ", ss, hit[0][ss])
	}
	fmt.Println("----------")
	for ss := 0; ss < len(hit[1]); ss++ {
		// fmt.Println("4連線: ", ss, float32(Hit[0][ss])/float32(round))
		fmt.Println("3連線: ", ss, hit[1][ss])
	}
	fmt.Println("----------")
	for ss := 0; ss < len(hit[2]); ss++ {
		// fmt.Println("5連線: ", ss, float32(Hit[1][ss])/float32(round))
		fmt.Println("4連線: ", ss, hit[2][ss])
	}
	fmt.Println("----------")
	for ss := 0; ss < len(hit[3]); ss++ {
		// fmt.Println("5連線: ", ss, float32(Hit[1][ss])/float32(round))
		fmt.Println("5連線: ", ss, hit[3][ss])
	}
	fmt.Println("----------")

	fmt.Println("--------------------------------------")
	fmt.Println("--------------------------------------")

	fmt.Println("-----分數-----")
	for ss := 0; ss < len(score[0]); ss++ {
		fmt.Println("2連線: ", ss, float32(score[0][ss]))
	}
	fmt.Println("----------")
	for ss := 0; ss < len(score[1]); ss++ {
		fmt.Println("3連線: ", ss, float32(score[1][ss]))
	}
	fmt.Println("----------")
	for ss := 0; ss < len(score[2]); ss++ {
		fmt.Println("4連線: ", ss, float32(score[2][ss]))
	}
	fmt.Println("----------")
	for ss := 0; ss < len(score[3]); ss++ {
		fmt.Println("5連線: ", ss, float32(score[3][ss]))
	}
	fmt.Println("----------")

	fmt.Println("--------------------------------------")
	fmt.Println("--------------------------------------")

	// fmt.Println("-----RTP-----")
	// for ss := 0; ss < len(score[0]); ss++ {
	// 	fmt.Println("2連線: ", ss, float32(score[0][ss])/float32(round)/float32(1))
	// }
	// fmt.Println("----------")
	// for ss := 0; ss < len(score[1]); ss++ {
	// 	fmt.Println("3連線: ", ss, float32(score[1][ss])/float32(round)/float32(1))
	// }
	// fmt.Println("----------")
	// for ss := 0; ss < len(score[2]); ss++ {
	// 	fmt.Println("4連線: ", ss, float32(score[2][ss])/float32(round)/float32(1))
	// }
	// fmt.Println("----------")
	// for ss := 0; ss < len(score[3]); ss++ {
	// 	fmt.Println("5連線: ", ss, float32(score[3][ss])/float32(round)/float32(1))
	// }
	// fmt.Println("----------")

}

// GetAllFreeGameInfo 取得 Free Game 遊戲結果 _____窮舉
func GetAllFreeGameInfo(goldSym int) {
	round := 0
	var totalWin int = 0
	var normalWin int = 0
	var goldWin int = 0
	var goldSymbol int = goldSym
	// var reelIndex = GetReelIndex(mgFeatureWeig)

	for ss := 0; ss < len(FreeGameReel[goldSymbol-1][0]); ss++ {
		for sss := 0; sss < len(FreeGameReel[goldSymbol-1][1]); sss++ {
			for ssss := 0; ssss < len(FreeGameReel[goldSymbol-1][2]); ssss++ {
				for sssss := 0; sssss < len(FreeGameReel[goldSymbol-1][3]); sssss++ {
					for ssssss := 0; ssssss < len(FreeGameReel[goldSymbol-1][4]); ssssss++ {
						round++
						var result = GameResult{}
						var gameInfo = []GameInfo{}
						var tmp = GameInfo{}

						rngReult := []int{ss, sss, ssss, sssss, ssssss}
						//rngReult := []int{0, 10, 0, 3, 3}
						reelResult := GetNotSortSymbolsForFreeGame(rngReult, goldSymbol-1)
						// reelResult := [][]int{
						// 	[]int{1, 2, 3},
						// 	[]int{1, 4, 5},
						// 	[]int{1, 6, 7},
						// 	[]int{10, 6, 7},
						// 	[]int{9, 6, 7},
						// }
						tmp.Reel = reelResult

						tmp.WinLineInfo = GetWinLineInfo(tmp.Reel, 1, mgLinePos, goldSymbol)
						tmp.GoldWinLineInfo = GetGoldWinLineInfo(tmp.Reel, 1, fgLinePos, goldSymbol)
						tmp.FGWinInfo = GetScatterWinInfo(tmp.Reel, 1)
						gameInfo = append(gameInfo, tmp)

						result.TotalWin = GetTotalWin(gameInfo)
						result.Info = gameInfo
						result.Status = "NORMAL"

						//fmt.Println(result.TotalWin)
						totalWin += result.TotalWin
						normalWin += result.TotalWin
						normalWin -= result.Info[0].FGWinInfo.WinPoint

						if len(result.Info[0].GoldWinLineInfo) > 0 {
							goldWin += result.Info[0].GoldWinLineInfo[0].WinPoint
						}

						// if round > 1177599 {
						// 	fmt.Println(round)
						// }

						if result.TotalWin > 0 {
							for ii := 0; ii < len(tmp.WinLineInfo); ii++ {
								hit[tmp.WinLineInfo[ii].Count-2][tmp.WinLineInfo[ii].Symbol]++
								score[tmp.WinLineInfo[ii].Count-2][tmp.WinLineInfo[ii].Symbol] += tmp.WinLineInfo[ii].WinPoint
							}

						}

						if tmp.FGWinInfo.WinPoint > 0 {
							scHit++
						}

					}
				}
			}
		}
		fmt.Println(ss)
	}
	fmt.Println("GOLD:     ", goldSymbol)
	fmt.Println("round:    ", round)
	fmt.Println("SCATTER:  ", scHit)
	fmt.Println("Trigger:  ", float32(scHit)/float32(round))
	fmt.Println("rtp:      ", float32(totalWin)/float32(round)/float32(1))
	fmt.Println("gold_rtp: ", float32(goldWin)/float32(round)/float32(1))
	fmt.Println("sc_rtp:   ", float32(totalWin-normalWin)/float32(round)/float32(1))
	fmt.Println("normal_rtp:", float32(normalWin-goldWin)/float32(round)/float32(1))
	fmt.Println("TotalWin: ", totalWin)
	fmt.Println("NormalWin: ", normalWin)
	fmt.Println("--------------------")

	fmt.Println("-----HIT-----")
	for ss := 0; ss < len(hit[0]); ss++ {
		// fmt.Println("4連線: ", ss, float32(Hit[0][ss])/float32(round))
		fmt.Println("2連線: ", ss, hit[0][ss])
	}
	fmt.Println("----------")
	for ss := 0; ss < len(hit[1]); ss++ {
		// fmt.Println("4連線: ", ss, float32(Hit[0][ss])/float32(round))
		fmt.Println("3連線: ", ss, hit[1][ss])
	}
	fmt.Println("----------")
	for ss := 0; ss < len(hit[2]); ss++ {
		// fmt.Println("5連線: ", ss, float32(Hit[1][ss])/float32(round))
		fmt.Println("4連線: ", ss, hit[2][ss])
	}
	fmt.Println("----------")
	for ss := 0; ss < len(hit[3]); ss++ {
		// fmt.Println("5連線: ", ss, float32(Hit[1][ss])/float32(round))
		fmt.Println("5連線: ", ss, hit[3][ss])
	}
	fmt.Println("----------")

	fmt.Println("--------------------------------------")
	fmt.Println("--------------------------------------")

	fmt.Println("-----分數-----")
	for ss := 0; ss < len(score[0]); ss++ {
		fmt.Println("2連線: ", ss, float32(score[0][ss]))
	}
	fmt.Println("----------")
	for ss := 0; ss < len(score[1]); ss++ {
		fmt.Println("3連線: ", ss, float32(score[1][ss]))
	}
	fmt.Println("----------")
	for ss := 0; ss < len(score[2]); ss++ {
		fmt.Println("4連線: ", ss, float32(score[2][ss]))
	}
	fmt.Println("----------")
	for ss := 0; ss < len(score[3]); ss++ {
		fmt.Println("5連線: ", ss, float32(score[3][ss]))
	}
	fmt.Println("----------")

	fmt.Println("--------------------------------------")
	fmt.Println("--------------------------------------")
}

// GetOneRNGforEveryReelForNormalGame 取得 Normal Game 5輪分別的亂數
func GetOneRNGforEveryReelForNormalGame(rtp int) []int {
	result := []int{0, 0, 0, 0, 0}
	if rtp == 965 {
		for i := 0; i < len(result); i++ {
			result[i] = tool.GenerateRandNum(len(NormalGameReel965[i]))
		}
	} else if rtp == 95 {
		for i := 0; i < len(result); i++ {
			result[i] = tool.GenerateRandNum(len(NormalGameReel95[i]))
		}
	} else if rtp == 99 {
		for i := 0; i < len(result); i++ {
			result[i] = tool.GenerateRandNum(len(NormalGameReel99[i]))
		}
	}
	return result
}

// GetOneRNGforEveryReelForFreeGame 取得 Free Game 5輪分別的亂數
func GetOneRNGforEveryReelForFreeGame(index int) []int {
	result := []int{0, 0, 0, 0, 0}
	for i := 0; i < len(result); i++ {
		result[i] = tool.GenerateRandNum(len(FreeGameReel[index][i]))
	}
	return result
}

// GetNotSortSymbolsForNormalGame 取得轉輪表
func GetNotSortSymbolsForNormalGame(rng []int, rtp int) [][]int {
	result := [][]int{}

	if rtp == 965 {
		for reel := 0; reel < len(rng); reel++ {
			result = append(result, GetSingleReelSymbol(NormalGameReel965[reel], Column[MGINDEX], rng[reel]))
		}
	} else if rtp == 95 {
		for reel := 0; reel < len(rng); reel++ {
			result = append(result, GetSingleReelSymbol(NormalGameReel95[reel], Column[MGINDEX], rng[reel]))
		}
	} else if rtp == 99 {
		for reel := 0; reel < len(rng); reel++ {
			result = append(result, GetSingleReelSymbol(NormalGameReel99[reel], Column[MGINDEX], rng[reel]))
		}
	}
	return result
}

// GetNotSortSymbolsForFreeGame 取得轉輪表
func GetNotSortSymbolsForFreeGame(rng []int, index int) [][]int {
	result := [][]int{}

	for reel := 0; reel < len(rng); reel++ {
		result = append(result, GetSingleReelSymbol(FreeGameReel[index][reel], Column[FGINDEX], rng[reel]))
	}
	return result
}

// GetSingleReelSymbol 取得單輪的 3個獎圖
func GetSingleReelSymbol(gameReel []int, columnCount int, rng int) []int {
	result := []int{}

	for column := 0; column < columnCount; column++ {
		// result[column] = _gameReel[_rng+column]
		//fmt.Println(column)
		result = append(result, gameReel[(rng+column)%len(gameReel)])
	}
	return result
}

// GetWinLineInfo 取得總贏線資訊
func GetWinLineInfo(reel [][]int, gold int, linePos [][]int, goldSymbol int) []LineInfo {
	var tmp = []LineInfo{}

	for lineIndex := 0; lineIndex < len(linePos); lineIndex++ {
		result := LineInfo{}
		tmpLinePos := linePos[lineIndex]

		if reel[0][tmpLinePos[0]] != WildNumber {
			result = SymbolLineInfo(reel, tmpLinePos, reel[0][tmpLinePos[0]])
		} else {
			result = WildLineInfo(reel, tmpLinePos)
		}

		// symbol 不等於 Scatter
		// 不跳過黃金獎圖
		if result.WinPoint > 0 {
			result.LineIndex = lineIndex
			result.WinPoint *= gold

			tmp = append(tmp, result)
		}
	}

	if len(tmp) > 0 {
		return tmp
	}
	return nil
}

// GetGoldWinLineInfo 取得黃金獎圖贏線資訊
func GetGoldWinLineInfo(reel [][]int, gold int, linePos [][]int, goldSymbol int) []LineInfo {
	var tmp = []LineInfo{}
	var count int = 0
	var pay int = 0

	for reelCount := 0; reelCount < len(reel); reelCount++ {
		for columnCount := 0; columnCount < len(reel[reelCount]); columnCount++ {
			if reel[reelCount][columnCount] == goldSymbol {
				count++
				break
			}
		}
	}

	pay = payTable[goldSymbol][count]
	if pay > 0 {
		result := LineInfo{}
		result.Symbol = goldSymbol
		result.Count = count
		result.WinPoint = pay * len(linePos) * gold
		tmp = append(tmp, result)
	}

	if len(tmp) > 0 {
		return tmp
	}
	return nil
}

// GetSymbolCountEveryReel 取得每輪符合目標獎圖的數量
func GetSymbolCountEveryReel(_reel [][]int, _symbol int) []int {
	var result = []int{0, 0, 0, 0, 0}

	for reelCount := 0; reelCount < len(_reel); reelCount++ {
		for columnCount := 0; columnCount < len(_reel[reelCount]); columnCount++ {
			if _reel[reelCount][columnCount] == _symbol {
				result[reelCount]++
			}
		}
	}
	return result
}

// SymbolLineInfo 一般獎圖(非WILD的賠率)的連線
func SymbolLineInfo(reel [][]int, linePos []int, targetSymbol int) LineInfo {
	rowIndex := 1
	result := LineInfo{}

	for rowIndex < len(linePos) {
		if reel[rowIndex][linePos[rowIndex]] == targetSymbol || reel[rowIndex][linePos[rowIndex]] == WildNumber {
			rowIndex++
		} else {
			//rowIndex--
			break
		}
	}

	result.Symbol = targetSymbol
	result.Count = rowIndex
	result.WinPoint = payTable[targetSymbol][rowIndex]
	return result
}

// WildLineInfo WILD(WILD和一般獎圖比較)的連線
func WildLineInfo(reel [][]int, linePos []int) LineInfo {
	rowIndex := 1
	nowSymbol := WildNumber

	onlyWild := LineInfo{}
	normal := LineInfo{}

	for rowIndex < len(linePos) {
		if reel[rowIndex][linePos[rowIndex]] == nowSymbol {
			rowIndex++
		} else {
			//rowIndex--
			break
		}
	}

	// wild 沒有連線分數
	onlyWild.Symbol = nowSymbol
	onlyWild.Count = rowIndex
	onlyWild.WinPoint = payTable[nowSymbol][rowIndex]

	rowIndex = 1

	for rowIndex < len(linePos) {
		if nowSymbol != WildNumber && reel[rowIndex][linePos[rowIndex]] != WildNumber && reel[rowIndex][linePos[rowIndex]] != nowSymbol {
			//rowIndex--
			break
		} else {
			if nowSymbol == WildNumber && reel[rowIndex][linePos[rowIndex]] != WildNumber {
				nowSymbol = reel[rowIndex][linePos[rowIndex]]
			}
			rowIndex++
		}
	}

	normal.Symbol = nowSymbol
	normal.Count = rowIndex
	normal.WinPoint = payTable[nowSymbol][rowIndex]
	// if normal.Symbol == ScatterNumber {
	// 	normal.WinPoint = 0
	// }

	if onlyWild.WinPoint > normal.WinPoint {
		return onlyWild
	}
	return normal
}

// GetScatterWinInfo 判斷 Scatter 連線資訊
func GetScatterWinInfo(reel [][]int, gold int) LineInfo {
	scCount := 0
	result := LineInfo{}

	for reelCount := 0; reelCount < len(reel); reelCount++ {
		for columnCount := 0; columnCount < len(reel[reelCount]); columnCount++ {
			if reel[reelCount][columnCount] == ScatterNumber {
				scCount++
			}
		}
	}

	if scCount >= 3 {
		result.Symbol = ScatterNumber
		result.Count = scCount
		result.WinPoint = TotalBet * gold * payTable[11][scCount]
		return result
	}
	return result
}

// GetTotalWin 計算當局總分
func GetTotalWin(gameInfo []GameInfo) int {
	totalWin := 0

	for index := 0; index < len(gameInfo); index++ {
		for winLineIndex := 0; winLineIndex < len(gameInfo[index].WinLineInfo); winLineIndex++ {
			totalWin += gameInfo[index].WinLineInfo[winLineIndex].WinPoint
		}
		for goldWinLineIndex := 0; goldWinLineIndex < len(gameInfo[index].GoldWinLineInfo); goldWinLineIndex++ {
			totalWin += gameInfo[index].GoldWinLineInfo[goldWinLineIndex].WinPoint
		}
		totalWin += gameInfo[index].FGWinInfo.WinPoint
	}
	return totalWin
}

// CheckIsRepeat 檢查獎勵圖是否重複
func CheckIsRepeat(lineInfo []LineInfo, checkSymbol int) bool {
	for count := 0; count < len(lineInfo); count++ {
		if lineInfo[count].Symbol == checkSymbol {
			return true
		}
	}
	return false
}

func printString(win int) string {
	if win > 0 {
		return "      ( V )"
	}
	return ""
}

// GameResult -> 遊戲總結果
type GameResult struct {
	TotalWin   int
	Info       []GameInfo
	Status     string
	GoldSymbol int
}

// GameInfo  當局結果
type GameInfo struct {
	Reel            [][]int
	WinLineInfo     []LineInfo
	GoldWinLineInfo []LineInfo
	FGWinInfo       LineInfo
}

// LineInfo  連線資訊
type LineInfo struct {
	Symbol    int
	WinPoint  int
	Count     int
	LineIndex int
}

// ＭultipleData 選擇資料
type ＭultipleData struct {
	Value  []int
	Weight []int
}
