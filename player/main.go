package main

import (
	"fmt"
	"math"
	"math/rand"
	game "slotGame/tooRich/game"
	"sort"
	"strconv"
	"time"

	"github.com/Luxurioust/excelize"
)

//理論 RTP
var theoremRTP float32 = 0.965

//理論 標準差
var theoremStd float32 = 3.0734

var allRTP float32 = 0
var allNGRTP float32 = 0
var allFGRTP float32 = 0
var allTIME int = 0

func main() {
	// fmt.Printf("last rtn: %d\n", funcA(5))

	// goroutine()
	// goroutine2()
	// goroutine3()
	goroutine4()
	// goroutine5()

	// //////////////// 分析效能 ////////////////
	// f, _ := os.OpenFile("cpu.profile", os.O_CREATE|os.O_RDWR, 0644)
	// defer f.Close()
	// pprof.StartCPUProfile(f)
	// defer pprof.StopCPUProfile()

	// PlayAndMultipleAndVariance(1000000)
	// //////////////// 分析效能 ////////////////

	// result := game.GetFixNormalGameInfo()
	// fmt.Println(result.Info[0].WinLineInfo)
	// game.GetAllNormalGameInfo()
	// game.GetAllFreeGameInfo(2)

	// PlayAndMultipleAndVarianceForRTP(10000, 100000)
	// PlayAndMultipleAndVarianceForRTP(10000, 200000)
	// PlayAndMultipleAndVarianceForRTP(10000, 300000)
	// PlayAndMultipleAndVarianceForRTP(10000, 400000)
	// PlayAndMultipleAndVarianceForRTP(10000, 500000)

	// fmt.Println("-----------------DONE-----------------")

	// getMinQ1Q2Q3Max([]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13})

	for ii := 0; ii < 2; ii++ {
		// PlayAndMultipleAndVariance(10000000)
		// SurvivalRoundAndWinLoseRate(40000, 100, 965)
		// SurvivalRoundAndWinLoseRate(40000, 100, 95)
		// SurvivalRoundAndWinLoseRate(40000, 100, 99)
		// SurvivalRoundAndWinLoseRate(40000, 50, 965)
		// SurvivalRoundAndWinLoseRate(40000, 50, 95)
		// SurvivalRoundAndWinLoseRate(40000, 50, 99)
	}

	// // 跑 rtp 震盪
	// for jj := 0; jj < 1; jj++ {
	// 	var time int = 1000
	// 	var spin int = 10000
	// 	fmt.Println("----------------------------------------------------------------------------------------------------")
	// 	fmt.Println("----------------------------------------------------------------------------------------------------")
	// 	fmt.Println("----------------------------------------------------------------------------------------------------")
	// 	fmt.Println("----------------------------------------------------------------------------------------------------")
	// 	fmt.Println("----------------------------------------------------------------------------------------------------")
	// 	fmt.Println("----------------------------------------------------------------------------------------------------")
	// 	fmt.Println("跑的次數: ", time)
	// 	fmt.Println("跑的局數: ", spin)
	// 	// fmt.Println("Time", "TotalRtp", "MgRtp", "FgRtp", "Trigger", "FgMult.")
	// 	fmt.Println("Time", "TotalRtp")
	// 	PlayAndMultipleAndVarianceForRTP(time, spin)
	// }

}

func goroutine() {
	// https://ithelp.ithome.com.tw/articles/10256509
	c := make(chan int)
	var spinTime = 100000

	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine(1000, spinTime, c)

	x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 := <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c
	fmt.Println("太有財!!!")
	fmt.Println(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
	fmt.Println((x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10))
}

func goroutine2() {
	// https://ithelp.ithome.com.tw/articles/10256509
	c := make(chan int)
	var spinTime = 200000

	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine2(1000, spinTime, c)

	x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 := <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c
	fmt.Println("太有財!!!")
	fmt.Println(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
	fmt.Println((x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10))
}

func goroutine3() {
	// https://ithelp.ithome.com.tw/articles/10256509
	c := make(chan int)
	var spinTime = 300000

	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine3(1000, spinTime, c)

	x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 := <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c
	fmt.Println("太有財!!!")
	fmt.Println(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
	fmt.Println((x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10))
}

func goroutine4() {
	// https://ithelp.ithome.com.tw/articles/10256509
	c := make(chan int)
	var round = 250
	var spinTime = 400000

	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)

	x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 := <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c
	fmt.Println("太有財!!!")
	fmt.Println(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
	fmt.Println((x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10))
}

func goroutine5() {
	// https://ithelp.ithome.com.tw/articles/10256509
	c := make(chan int)
	var round = 250
	var spinTime = 500000

	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)
	go PlayAndMultipleAndVarianceGoroutine4(round, spinTime, c)

	x1, x2, x3, x4, x5, x6, x7, x8, x9, x10 := <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c, <-c
	fmt.Println("太有財!!!")
	fmt.Println(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10)
	fmt.Println((x1 + x2 + x3 + x4 + x5 + x6 + x7 + x8 + x9 + x10))
}

// func funcA(n int) int {
// 	if n == 0 {
// 		return 0
// 	}
// 	fmt.Printf("resursive: %d\n", n)
// 	r := funcA(n - 1)
// 	fmt.Printf("reverse resursive: %d\n", n)

// 	return r
// }

// PlayAndMultipleAndVariance 流程, 倍數, 變異數
func PlayAndMultipleAndVariance(playRound int) {
	spendTime := time.Now()

	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = playRound
	var totalBet = 10
	var gold int = 1
	var NGScore = 0
	var FGScore = 0
	var goldScore = 0
	var intoFG = 0
	var fgRound = 0
	var result = game.GameResult{}
	var totalRTP float32 = 0
	var RTPError float64 = 0
	var NGHit = 0
	var FGHit = 0
	var fgTrig = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	//Normal Game 倍數 含左不含右
	var MultipleNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	//Normal Game rtp
	var RTPNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	//Free Game 倍數 含左不含右
	var MultipleFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	//Free Game rtp
	var RTPFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	// FG 一局一局算
	//Free Game 倍數 含左不含右
	var MultipleFGSingle []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	//Free Game rtp
	var RTPFGSingle []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

	var value []int
	// var higherReel []westGame.GameResult
	var bar string = ""

	for time := 0; time < round; {
		// 顯示進度
		if time%(round/20) == 0 && time != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-20s]", bar)
		}

		if result.Status == "" || result.Status == "NORMAL" {
			result = game.GetNormalGameInfo(965)
			NGScore += result.TotalWin
			if result.Status == "FREE" {
				intoFG++
				// result.Status = "NORMAL"
			}

			if result.TotalWin > 0 {
				NGHit++
				// if result.TotalWin > 300 {
				// 	// fmt.Println(result.Info[0].Reel)
				// 	tool.PrintReel3x5(result.Info[0].Reel)
				// 	fmt.Println(result.TotalWin)
				// 	fmt.Println(result.Info[0].WinLineInfo)
				// 	fmt.Println("========================================================")
				// }
			}

			GetMultiple(result.TotalWin, MultipleNG, RTPNG, totalBet*gold)
			value = append(value, result.TotalWin)

			time++
		} else if result.Status == "FREE" {
			result = game.GetFreeGameInfo()
			FGScore += result.TotalWin
			fgRound += len(result.Info)
			fgTrig[len(result.Info)/10-1]++

			for ii := 0; ii < len(result.Info); ii++ {
				if len(result.Info[ii].WinLineInfo) > 0 || result.Info[ii].FGWinInfo.WinPoint > 0 {
					FGHit++
				}

				if len(result.Info[ii].GoldWinLineInfo) > 0 {
					goldScore += result.Info[ii].GoldWinLineInfo[0].WinPoint
				}

				GetMultiple(CalScoreWithSC(result.Info[ii]), MultipleFGSingle, RTPFGSingle, totalBet*gold)
			}

			// 倍數
			GetMultiple(result.TotalWin, MultipleFG, RTPFG, totalBet*gold)
			value[time-1] += result.TotalWin
		} else {
			fmt.Println("狀態錯誤:  " + result.Status)
		}
	}

	allTIME++
	totalRTP = float32(NGScore+FGScore) / float32(round) / float32(totalBet)
	RTPError = math.Abs(float64(totalRTP) - float64(theoremRTP))

	allRTP += totalRTP
	allNGRTP += float32(NGScore) / float32(round) / float32(totalBet)
	allFGRTP += float32(FGScore) / float32(round) / float32(totalBet)

	fmt.Println("")
	fmt.Println("太有財!!!")
	fmt.Println("@基本數值")
	fmt.Println("總RTP:         ", totalRTP, "(誤差:", fmt.Sprintf("%.6f", RTPError), ")")
	fmt.Println("MG_RTP:        ", float32(NGScore)/float32(round)/float32(totalBet))
	fmt.Println("FG_RTP:        ", float32(FGScore)/float32(round)/float32(totalBet))
	fmt.Println("FG_Symb_RTP:      ", float32(FGScore-goldScore)/float32(round)/float32(totalBet))
	fmt.Println("FG_Gold_RTP:      ", float32(goldScore)/float32(round)/float32(totalBet))
	fmt.Println("NG 觸發 FG:     ", float32(intoFG)/float32(round))
	fmt.Println("FG平均局數:      ", float32(fgRound)/float32(intoFG))
	fmt.Println("NGHit:         ", float32(NGHit)/float32(round))
	fmt.Println("FGHit:         ", float32(FGHit)/float32(fgRound))
	fmt.Println("------------------------")
	fmt.Println("  ")

	fmt.Println("FG總局數比例")
	var tmp int = getTotal(fgTrig)
	for i := 0; i < len(fgTrig); i++ {
		fmt.Println(10*(i+1), "局: ", float32(fgTrig[i])/float32(tmp))
	}
	fmt.Println("------------------------")
	fmt.Println("------------------------")

	fmt.Println("@倍數區間")
	PrintMultiple(round, MultipleNG, "＋Normal Game")
	PrintMultiple(intoFG, MultipleFG, "＋Free Game")
	PrintMultiple(fgRound, MultipleFGSingle, "＋Free Game Single")
	fmt.Println("------------------------")

	fmt.Println("@RTP區間")
	PrintRTP(round, RTPNG, totalBet, gold, NGScore, "+Normal Game")
	PrintRTP(round, RTPFG, totalBet, gold, FGScore, "+Free Game")
	fmt.Println("------------------------")

	fmt.Println("@變異數")
	PrintVariance(GetVariance(value, totalBet, float32(NGScore+FGScore)/float32(round)/float32(totalBet)))

	fmt.Println("累計TIME:        ", allTIME)
	fmt.Println("累計RTP:         ", float32(allRTP)/float32(allTIME))
	fmt.Println("累計NG_RTP:      ", float32(allNGRTP)/float32(allTIME))
	fmt.Println("累計FG_RTP:      ", float32(allFGRTP)/float32(allTIME))
	fmt.Println("------------------------")

	elapsed := time.Since(spendTime)
	fmt.Println("＠花費時間: ", elapsed)
	fmt.Println("------------------------")
	fmt.Println("------------------------")
}

// PlayAndMultipleAndVarianceForRTP 玩家RTP震盪
func PlayAndMultipleAndVarianceForRTP(bigRound int, smallRound int) {
	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = smallRound
	var totalBet = 10
	var gold int = 1
	var over100rtp []float32
	var value []int
	var bar string = ""
	// var totalBetCollect int
	// var totalPayCollect int
	// var rtpCollect []float32

	for ii := 0; ii < bigRound; ii++ {
		var NGScore = 0
		var FGScore = 0
		var intoFG = 0
		var fgRound = 0
		var result = game.GameResult{}
		var totalRTP float32 = 0
		var NGHit = 0
		var FGHit = 0

		//Normal Game 倍數 含左不含右
		var MultipleNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Normal Game rtp
		var RTPNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game 倍數 含左不含右
		var MultipleFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game rtp
		var RTPFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		// 顯示進度
		if ii%(bigRound/50) == 0 && ii != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-50s]", bar)
		}
		for time := 0; time < round; {
			if result.Status == "" || result.Status == "NORMAL" {
				result = game.GetNormalGameInfo(965)
				NGScore += result.TotalWin
				// totalBetCollect += totalBet
				// totalPayCollect += result.TotalWin
				if result.Status == "FREE" {
					intoFG++
				}
				if result.TotalWin > 0 {
					NGHit++
				}
				GetMultiple(result.TotalWin, MultipleNG, RTPNG, totalBet*gold)
				value = append(value, result.TotalWin)

				time++
			} else if result.Status == "FREE" {
				result = game.GetFreeGameInfo()
				FGScore += result.TotalWin
				// totalPayCollect += result.TotalWin
				fgRound += len(result.Info)

				for ii := 0; ii < len(result.Info); ii++ {
					if len(result.Info[ii].WinLineInfo) > 0 || result.Info[ii].FGWinInfo.WinPoint > 0 {
						FGHit++
					}
				}

				// 倍數
				GetMultiple(result.TotalWin, MultipleFG, RTPFG, totalBet*gold)
				value[time-1] += result.TotalWin
			} else {
				fmt.Println("狀態錯誤:  " + result.Status)
			}
		}
		totalRTP = float32(NGScore+FGScore) / float32(round) / float32(totalBet)
		if totalRTP >= 1 {
			over100rtp = append(over100rtp, totalRTP)
		}
		// fmt.Println(bigRound+1, totalRTP)

		// if (ii+1)%1 == 0 {
		// 	rtpCollect = append(rtpCollect, float32(totalPayCollect)/float32(totalBetCollect))
		// }
	}
	fmt.Println("")
	// fmt.Println("破百的rtp      ", over100rtp)
	fmt.Println("總共跑幾次:    ", bigRound)
	fmt.Println("每次跑幾局:    ", smallRound)
	fmt.Println("破百次數:      ", len(over100rtp))
	// // 寫進Excel
	// sliceToExcel(rtpCollect)
	fmt.Println("==============================================")
}

// FixPlay 工程盤面
func FixPlay() {
	var result = game.GameResult{}
	fmt.Println(result)

	result = game.GetFixNormalGameInfo(965)
	result = game.GetFreeGameInfo()
	fmt.Println("Fix end")
}

// SurvivalRoundAndWinLoseRate 存活局數與輸贏比例
// 統計"玩到沒錢"或"目前的錢是初始錢的2倍以上" 的總局數
// "玩到沒錢"與"目前的錢是初始錢的2倍以上" 的比例
func SurvivalRoundAndWinLoseRate(round int, walletMultiple int, rtp int) {
	var bet int = 10
	var multiple int = walletMultiple
	var wallet int = bet * multiple
	var runRound = round
	var playRound int = 0
	var playRoundSlice = []int{}
	var winRoundSlice = []int{}
	var loseRoundSlice = []int{}
	var result = game.GameResult{}

	for count := 0; count < runRound; count++ {
		if count%(runRound/10) == 0 {
			fmt.Println(count / (runRound / 10))
		}
		for wallet >= bet {
			wallet -= bet
			playRound++
			if result.Status == "" || result.Status == "NORMAL" {
				result = game.GetNormalGameInfo(rtp)
				wallet += result.TotalWin

				if wallet >= bet*multiple*2 {
					winRoundSlice = append(winRoundSlice, playRound)
					break
				} else if wallet < bet {
					loseRoundSlice = append(loseRoundSlice, playRound)
					break
				}
			} else if result.Status == "FREE" {
				result = game.GetFreeGameInfo()
				wallet += result.TotalWin

				if wallet >= bet*multiple*2 {
					winRoundSlice = append(winRoundSlice, playRound)
					break
				} else if wallet < bet {
					loseRoundSlice = append(loseRoundSlice, playRound)
					break
				}
			} else {
				fmt.Println("狀態錯誤:  " + result.Status)
			}
		}
		playRoundSlice = append(playRoundSlice, playRound)
		wallet = bet * multiple
		playRound = 0
	}

	sort.Ints(playRoundSlice)
	sort.Ints(loseRoundSlice)
	sort.Ints(winRoundSlice)
	fmt.Println(len(playRoundSlice))
	fmt.Println(len(loseRoundSlice))

	fmt.Println("")
	fmt.Println("跑的次數:    ", runRound)
	fmt.Println("下注:        ", bet)
	fmt.Println("下注倍數:    ", multiple)
	fmt.Println("RTP:        ", rtp)
	fmt.Println("@存活局數:   ")
	fmt.Println("min :       ", playRoundSlice[0])
	fmt.Println("Q1  :       ", float32(playRoundSlice[2499]+playRoundSlice[2500])/float32(2))
	fmt.Println("Q2  :       ", float32(playRoundSlice[4999]+playRoundSlice[5000])/float32(2))
	fmt.Println("Q3  :       ", float32(playRoundSlice[7499]+playRoundSlice[7500])/float32(2))
	fmt.Println("Max:        ", playRoundSlice[9999])
	fmt.Println("@輸贏比例:   ")
	fmt.Println("贏2倍存活局數:")
	fmt.Println("贏兩倍資料長度:", len(winRoundSlice))
	getMinQ1Q2Q3Max(winRoundSlice)
	fmt.Println("贏:輸 ==", len(winRoundSlice), ":", len(loseRoundSlice))
	fmt.Println("----------------------------------------------------------------------------------------")
}

// GetMultiple 取得倍數區間
func GetMultiple(score int, slice []int, RTPSlice []int, totalBet int) {
	if score == totalBet*0 {
		slice[0]++
		RTPSlice[0] += score
	} else if score > totalBet*0 && score < totalBet*1 { // 0~1
		slice[1]++
		RTPSlice[1] += score
	} else if score >= totalBet*1 && score < totalBet*5 { // 1~5
		slice[2]++
		RTPSlice[2] += score
	} else if score >= totalBet*5 && score < totalBet*10 { // 5~10
		slice[3]++
		RTPSlice[3] += score
	} else if score >= totalBet*10 && score < totalBet*30 { // 10~30
		slice[4]++
		RTPSlice[4] += score
	} else if score >= totalBet*30 && score < totalBet*50 { // 30~50
		slice[5]++
		RTPSlice[5] += score
	} else if score >= totalBet*50 && score < totalBet*100 { // 50~100
		slice[6]++
		RTPSlice[6] += score
	} else if score >= totalBet*100 { // 100~
		slice[7]++
		RTPSlice[7] += score
	}
}

// GetVariance 取得變異數
func GetVariance(value []int, totalBet int, rtp float32) float32 {
	var result float32
	for index := 0; index < len(value); index++ {
		tmp := (float32(value[index]/totalBet) - rtp) * (float32(value[index]/totalBet) - rtp)
		result += tmp
	}
	fmt.Println("理論值:模擬值", "==", theoremRTP, ":", rtp)
	return result / (float32)(len(value))
}

// PrintMultiple 印出倍數區間
func PrintMultiple(round int, multipleSlice []int, title string) {
	tmp := 0
	for index := 0; index < len(multipleSlice); index++ {
		tmp += multipleSlice[index]
	}

	fmt.Println(title, "倍數區間:  ")
	fmt.Println("次數比較: ", round, " : ", tmp)
	fmt.Println("沒有得分:          ", float32(multipleSlice[0])/float32(round))
	fmt.Println("不含0 - 未滿1:     ", float32(multipleSlice[1])/float32(round))
	fmt.Println("包含1 - 不含5:     ", float32(multipleSlice[2])/float32(round))
	fmt.Println("包含5 - 不含10:    ", float32(multipleSlice[3])/float32(round))
	fmt.Println("包含10 - 不含30:   ", float32(multipleSlice[4])/float32(round))
	fmt.Println("包含30 - 不含50:   ", float32(multipleSlice[5])/float32(round))
	fmt.Println("包含50 - 不含100:  ", float32(multipleSlice[6])/float32(round))
	fmt.Println("包含100(以上):     ", float32(multipleSlice[7])/float32(round))
	fmt.Println("  ")
}

// PrintRTP 印出 RTP 區間
func PrintRTP(round int, RTPSlice []int, bet int, gold int, gameScore int, title string) {
	tmp := 0
	for index := 0; index < len(RTPSlice); index++ {
		tmp += RTPSlice[index]
	}

	fmt.Println(title, "RTP區間:  ")
	fmt.Println("分數比較: ", gameScore, " : ", tmp)
	fmt.Println("沒有得分:          ", float32(RTPSlice[0])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("不含0 - 未滿1:     ", float32(RTPSlice[1])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("包含1 - 不含5:     ", float32(RTPSlice[2])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("包含5 - 不含10:    ", float32(RTPSlice[3])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("包含10 - 不含30:   ", float32(RTPSlice[4])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("包含30 - 不含50:   ", float32(RTPSlice[5])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("包含50 - 不含100:   ", float32(RTPSlice[6])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("包含100(以上):     ", float32(RTPSlice[7])/float32(round)/float32(bet)/float32(gold))
	fmt.Println("  ")
}

// PrintVariance 印出變異數, 標準差
func PrintVariance(variance float32) {
	fmt.Println("變異數: ", variance)
	fmt.Println("標準差: ", math.Sqrt(float64(variance)), "(理論值:", theoremStd, ")")
	fmt.Println("  ")
}

// CalScoreNoSC 計算每局不含sc分數
func CalScoreNoSC(info game.GameInfo) int {
	if len(info.WinLineInfo) > 0 {
		var result int = 0
		for ii := 0; ii < len(info.WinLineInfo); ii++ {
			result += info.WinLineInfo[ii].WinPoint
		}
		return result
	}
	return 0
}

// CalScoreWithSC 計算每局不含sc分數
func CalScoreWithSC(info game.GameInfo) int {
	if len(info.WinLineInfo) > 0 {
		var result int = 0
		for ii := 0; ii < len(info.WinLineInfo); ii++ {
			result += info.WinLineInfo[ii].WinPoint
		}
		result += info.FGWinInfo.WinPoint
		return result
	}
	return 0
}

func getTotal(source []int) int {
	var result int = 0
	for i := 0; i < len(source); i++ {
		result += source[i]
	}
	return result
}

func getMinQ1Q2Q3Max(dataSlice []int) {
	var Q1 float32 = 0
	var Q2 float32 = 0
	var Q3 float32 = 0
	var length = len(dataSlice)

	// Q1
	if (length*1)%4 == 0 {
		// 整除取2個平均
		var index = length/4 + 1
		Q1 = float32((dataSlice[index])+dataSlice[index+1]) / 2
	} else {
		var index = length / 4
		Q1 = float32((dataSlice[index]))
	}

	// Q2
	if (length*2)%4 == 0 {
		// 整除取2個平均
		var index = length*2/4 - 1
		Q2 = float32((dataSlice[index])+dataSlice[index+1]) / 2
	} else {
		var index = length * 2 / 4
		Q2 = float32((dataSlice[index]))
	}

	// Q3
	if (length*3)%4 == 0 {
		// 整除取2個平均
		var index = length*3/4 + 1
		Q3 = float32((dataSlice[index])+dataSlice[index+1]) / 2
	} else {
		var index = length * 3 / 4
		Q3 = float32((dataSlice[index]))
	}

	fmt.Println("@存活局數: ")
	fmt.Println("min :     ", dataSlice[0])
	fmt.Println("Q1  :     ", Q1)
	fmt.Println("Q2  :     ", Q2)
	fmt.Println("Q3  :     ", Q3)
	fmt.Println("Max:      ", dataSlice[len(dataSlice)-1])

}

// sliceToExcel 陣列轉成Excel
func sliceToExcel(data []float32) {
	// 寫進excel
	f := excelize.NewFile()
	// 創建一個工作表
	index := f.NewSheet("Sheet2")
	for ii := 0; ii < len(data); ii++ {
		f.SetCellValue("Sheet2", "A"+strconv.Itoa(ii+1), data[ii])
	}
	// 設置活頁簿的默認工作表
	f.SetActiveSheet(index)
	// 根據指定路徑保存活頁簿
	if err := f.SaveAs("Book_1202.xlsx"); err != nil {
		fmt.Println(err)
	}
}

// PlayAndMultipleAndVarianceGoroutine 跑
func PlayAndMultipleAndVarianceGoroutine(bigRound int, smallRound int, c chan int) {
	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = smallRound
	var totalBet = 10
	var gold int = 1
	var over100rtp []float32
	var value []int
	var bar string = ""
	// var totalBetCollect int
	// var totalPayCollect int
	// var rtpCollect []float32

	for ii := 0; ii < bigRound; ii++ {
		var NGScore = 0
		var FGScore = 0
		var intoFG = 0
		var fgRound = 0
		var result = game.GameResult{}
		var totalRTP float32 = 0
		var NGHit = 0
		var FGHit = 0

		//Normal Game 倍數 含左不含右
		var MultipleNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Normal Game rtp
		var RTPNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game 倍數 含左不含右
		var MultipleFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game rtp
		var RTPFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		// 顯示進度
		if ii%(bigRound/50) == 0 && ii != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-50s]", bar)
		}
		for time := 0; time < round; {
			if result.Status == "" || result.Status == "NORMAL" {
				result = game.GetNormalGameInfo(965)
				NGScore += result.TotalWin
				// totalBetCollect += totalBet
				// totalPayCollect += result.TotalWin
				if result.Status == "FREE" {
					intoFG++
				}
				if result.TotalWin > 0 {
					NGHit++
				}
				GetMultiple(result.TotalWin, MultipleNG, RTPNG, totalBet*gold)
				value = append(value, result.TotalWin)

				time++
			} else if result.Status == "FREE" {
				result = game.GetFreeGameInfo()
				FGScore += result.TotalWin
				// totalPayCollect += result.TotalWin
				fgRound += len(result.Info)

				for ii := 0; ii < len(result.Info); ii++ {
					if len(result.Info[ii].WinLineInfo) > 0 || result.Info[ii].FGWinInfo.WinPoint > 0 {
						FGHit++
					}
				}

				// 倍數
				GetMultiple(result.TotalWin, MultipleFG, RTPFG, totalBet*gold)
				value[time-1] += result.TotalWin
			} else {
				fmt.Println("狀態錯誤:  " + result.Status)
			}
		}
		totalRTP = float32(NGScore+FGScore) / float32(round) / float32(totalBet)
		if totalRTP >= 1 {
			over100rtp = append(over100rtp, totalRTP)
		}
		// fmt.Println(bigRound+1, totalRTP)

		// if (ii+1)%1 == 0 {
		// 	rtpCollect = append(rtpCollect, float32(totalPayCollect)/float32(totalBetCollect))
		// }
	}

	// goroutine
	c <- len(over100rtp)

	fmt.Println("")
	// fmt.Println("破百的rtp      ", over100rtp)
	fmt.Println("總共跑幾次:    ", bigRound)
	fmt.Println("每次跑幾局:    ", smallRound)
	fmt.Println("破百次數:      ", len(over100rtp))
	// // 寫進Excel
	// sliceToExcel(rtpCollect)
	fmt.Println("==============================================")
}

// PlayAndMultipleAndVarianceGoroutine2 跑
func PlayAndMultipleAndVarianceGoroutine2(bigRound int, smallRound int, c chan int) {
	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = smallRound
	var totalBet = 10
	var gold int = 1
	var over100rtp []float32
	var value []int
	var bar string = ""
	// var totalBetCollect int
	// var totalPayCollect int
	// var rtpCollect []float32

	for ii := 0; ii < bigRound; ii++ {
		var NGScore = 0
		var FGScore = 0
		var intoFG = 0
		var fgRound = 0
		var result = game.GameResult{}
		var totalRTP float32 = 0
		var NGHit = 0
		var FGHit = 0

		//Normal Game 倍數 含左不含右
		var MultipleNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Normal Game rtp
		var RTPNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game 倍數 含左不含右
		var MultipleFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game rtp
		var RTPFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		// 顯示進度
		if ii%(bigRound/50) == 0 && ii != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-50s]", bar)
		}
		for time := 0; time < round; {
			if result.Status == "" || result.Status == "NORMAL" {
				result = game.GetNormalGameInfo(965)
				NGScore += result.TotalWin
				// totalBetCollect += totalBet
				// totalPayCollect += result.TotalWin
				if result.Status == "FREE" {
					intoFG++
				}
				if result.TotalWin > 0 {
					NGHit++
				}
				GetMultiple(result.TotalWin, MultipleNG, RTPNG, totalBet*gold)
				value = append(value, result.TotalWin)

				time++
			} else if result.Status == "FREE" {
				result = game.GetFreeGameInfo()
				FGScore += result.TotalWin
				// totalPayCollect += result.TotalWin
				fgRound += len(result.Info)

				for ii := 0; ii < len(result.Info); ii++ {
					if len(result.Info[ii].WinLineInfo) > 0 || result.Info[ii].FGWinInfo.WinPoint > 0 {
						FGHit++
					}
				}

				// 倍數
				GetMultiple(result.TotalWin, MultipleFG, RTPFG, totalBet*gold)
				value[time-1] += result.TotalWin
			} else {
				fmt.Println("狀態錯誤:  " + result.Status)
			}
		}
		totalRTP = float32(NGScore+FGScore) / float32(round) / float32(totalBet)
		if totalRTP >= 1 {
			over100rtp = append(over100rtp, totalRTP)
		}
		// fmt.Println(bigRound+1, totalRTP)

		// if (ii+1)%1 == 0 {
		// 	rtpCollect = append(rtpCollect, float32(totalPayCollect)/float32(totalBetCollect))
		// }
	}

	// goroutine
	c <- len(over100rtp)

	fmt.Println("")
	// fmt.Println("破百的rtp      ", over100rtp)
	fmt.Println("總共跑幾次:    ", bigRound)
	fmt.Println("每次跑幾局:    ", smallRound)
	fmt.Println("破百次數:      ", len(over100rtp))
	// // 寫進Excel
	// sliceToExcel(rtpCollect)
	fmt.Println("==============================================")
}

// PlayAndMultipleAndVarianceGoroutine3 跑
func PlayAndMultipleAndVarianceGoroutine3(bigRound int, smallRound int, c chan int) {
	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = smallRound
	var totalBet = 10
	var gold int = 1
	var over100rtp []float32
	var value []int
	var bar string = ""
	// var totalBetCollect int
	// var totalPayCollect int
	// var rtpCollect []float32

	for ii := 0; ii < bigRound; ii++ {
		var NGScore = 0
		var FGScore = 0
		var intoFG = 0
		var fgRound = 0
		var result = game.GameResult{}
		var totalRTP float32 = 0
		var NGHit = 0
		var FGHit = 0

		//Normal Game 倍數 含左不含右
		var MultipleNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Normal Game rtp
		var RTPNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game 倍數 含左不含右
		var MultipleFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game rtp
		var RTPFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		// 顯示進度
		if ii%(bigRound/50) == 0 && ii != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-50s]", bar)
		}
		for time := 0; time < round; {
			if result.Status == "" || result.Status == "NORMAL" {
				result = game.GetNormalGameInfo(965)
				NGScore += result.TotalWin
				// totalBetCollect += totalBet
				// totalPayCollect += result.TotalWin
				if result.Status == "FREE" {
					intoFG++
				}
				if result.TotalWin > 0 {
					NGHit++
				}
				GetMultiple(result.TotalWin, MultipleNG, RTPNG, totalBet*gold)
				value = append(value, result.TotalWin)

				time++
			} else if result.Status == "FREE" {
				result = game.GetFreeGameInfo()
				FGScore += result.TotalWin
				// totalPayCollect += result.TotalWin
				fgRound += len(result.Info)

				for ii := 0; ii < len(result.Info); ii++ {
					if len(result.Info[ii].WinLineInfo) > 0 || result.Info[ii].FGWinInfo.WinPoint > 0 {
						FGHit++
					}
				}

				// 倍數
				GetMultiple(result.TotalWin, MultipleFG, RTPFG, totalBet*gold)
				value[time-1] += result.TotalWin
			} else {
				fmt.Println("狀態錯誤:  " + result.Status)
			}
		}
		totalRTP = float32(NGScore+FGScore) / float32(round) / float32(totalBet)
		if totalRTP >= 1 {
			over100rtp = append(over100rtp, totalRTP)
		}
		// fmt.Println(bigRound+1, totalRTP)

		// if (ii+1)%1 == 0 {
		// 	rtpCollect = append(rtpCollect, float32(totalPayCollect)/float32(totalBetCollect))
		// }
	}

	// goroutine
	c <- len(over100rtp)

	fmt.Println("")
	// fmt.Println("破百的rtp      ", over100rtp)
	fmt.Println("總共跑幾次:    ", bigRound)
	fmt.Println("每次跑幾局:    ", smallRound)
	fmt.Println("破百次數:      ", len(over100rtp))
	// // 寫進Excel
	// sliceToExcel(rtpCollect)
	fmt.Println("==============================================")
}

// PlayAndMultipleAndVarianceGoroutine4 跑
func PlayAndMultipleAndVarianceGoroutine4(bigRound int, smallRound int, c chan int) {
	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = smallRound
	var totalBet = 10
	var gold int = 1
	var over100rtp []float32
	var value []int
	var bar string = ""
	// var totalBetCollect int
	// var totalPayCollect int
	// var rtpCollect []float32

	for ii := 0; ii < bigRound; ii++ {
		var NGScore = 0
		var FGScore = 0
		var intoFG = 0
		var fgRound = 0
		var result = game.GameResult{}
		var totalRTP float32 = 0
		var NGHit = 0
		var FGHit = 0

		//Normal Game 倍數 含左不含右
		var MultipleNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Normal Game rtp
		var RTPNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game 倍數 含左不含右
		var MultipleFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game rtp
		var RTPFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		// 顯示進度
		if ii%(bigRound/50) == 0 && ii != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-50s]", bar)
		}
		for time := 0; time < round; {
			if result.Status == "" || result.Status == "NORMAL" {
				result = game.GetNormalGameInfo(965)
				NGScore += result.TotalWin
				// totalBetCollect += totalBet
				// totalPayCollect += result.TotalWin
				if result.Status == "FREE" {
					intoFG++
				}
				if result.TotalWin > 0 {
					NGHit++
				}
				GetMultiple(result.TotalWin, MultipleNG, RTPNG, totalBet*gold)
				value = append(value, result.TotalWin)

				time++
			} else if result.Status == "FREE" {
				result = game.GetFreeGameInfo()
				FGScore += result.TotalWin
				// totalPayCollect += result.TotalWin
				fgRound += len(result.Info)

				for ii := 0; ii < len(result.Info); ii++ {
					if len(result.Info[ii].WinLineInfo) > 0 || result.Info[ii].FGWinInfo.WinPoint > 0 {
						FGHit++
					}
				}

				// 倍數
				GetMultiple(result.TotalWin, MultipleFG, RTPFG, totalBet*gold)
				value[time-1] += result.TotalWin
			} else {
				fmt.Println("狀態錯誤:  " + result.Status)
			}
		}
		totalRTP = float32(NGScore+FGScore) / float32(round) / float32(totalBet)
		if totalRTP >= 1 {
			over100rtp = append(over100rtp, totalRTP)
		}
		// fmt.Println(bigRound+1, totalRTP)

		// if (ii+1)%1 == 0 {
		// 	rtpCollect = append(rtpCollect, float32(totalPayCollect)/float32(totalBetCollect))
		// }
	}

	// goroutine
	c <- len(over100rtp)

	fmt.Println("")
	// fmt.Println("破百的rtp      ", over100rtp)
	fmt.Println("總共跑幾次:    ", bigRound)
	fmt.Println("每次跑幾局:    ", smallRound)
	fmt.Println("破百次數:      ", len(over100rtp))
	// // 寫進Excel
	// sliceToExcel(rtpCollect)
	fmt.Println("==============================================")
}

// PlayAndMultipleAndVarianceGoroutine5 跑
func PlayAndMultipleAndVarianceGoroutine5(bigRound int, smallRound int, c chan int) {
	// 種子不用每次都抽
	rand.Seed(time.Now().UnixNano())
	var round = smallRound
	var totalBet = 10
	var gold int = 1
	var over100rtp []float32
	var value []int
	var bar string = ""
	// var totalBetCollect int
	// var totalPayCollect int
	// var rtpCollect []float32

	for ii := 0; ii < bigRound; ii++ {
		var NGScore = 0
		var FGScore = 0
		var intoFG = 0
		var fgRound = 0
		var result = game.GameResult{}
		var totalRTP float32 = 0
		var NGHit = 0
		var FGHit = 0

		//Normal Game 倍數 含左不含右
		var MultipleNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Normal Game rtp
		var RTPNG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game 倍數 含左不含右
		var MultipleFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		//Free Game rtp
		var RTPFG []int = []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

		// 顯示進度
		if ii%(bigRound/50) == 0 && ii != 0 {
			// print("=")
			bar += "█"
			fmt.Printf("\r[%-50s]", bar)
		}
		for time := 0; time < round; {
			if result.Status == "" || result.Status == "NORMAL" {
				result = game.GetNormalGameInfo(965)
				NGScore += result.TotalWin
				// totalBetCollect += totalBet
				// totalPayCollect += result.TotalWin
				if result.Status == "FREE" {
					intoFG++
				}
				if result.TotalWin > 0 {
					NGHit++
				}
				GetMultiple(result.TotalWin, MultipleNG, RTPNG, totalBet*gold)
				value = append(value, result.TotalWin)

				time++
			} else if result.Status == "FREE" {
				result = game.GetFreeGameInfo()
				FGScore += result.TotalWin
				// totalPayCollect += result.TotalWin
				fgRound += len(result.Info)

				for ii := 0; ii < len(result.Info); ii++ {
					if len(result.Info[ii].WinLineInfo) > 0 || result.Info[ii].FGWinInfo.WinPoint > 0 {
						FGHit++
					}
				}

				// 倍數
				GetMultiple(result.TotalWin, MultipleFG, RTPFG, totalBet*gold)
				value[time-1] += result.TotalWin
			} else {
				fmt.Println("狀態錯誤:  " + result.Status)
			}
		}
		totalRTP = float32(NGScore+FGScore) / float32(round) / float32(totalBet)
		if totalRTP >= 1 {
			over100rtp = append(over100rtp, totalRTP)
		}
		// fmt.Println(bigRound+1, totalRTP)

		// if (ii+1)%1 == 0 {
		// 	rtpCollect = append(rtpCollect, float32(totalPayCollect)/float32(totalBetCollect))
		// }
	}

	// goroutine
	c <- len(over100rtp)

	fmt.Println("")
	// fmt.Println("破百的rtp      ", over100rtp)
	fmt.Println("總共跑幾次:    ", bigRound)
	fmt.Println("每次跑幾局:    ", smallRound)
	fmt.Println("破百次數:      ", len(over100rtp))
	// // 寫進Excel
	// sliceToExcel(rtpCollect)
	fmt.Println("==============================================")
}
